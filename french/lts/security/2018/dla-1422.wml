#use wml::debian::translation-check translation="ab3be4ee01879fd4484c795bbaa824377c218575" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pourraient conduire à une augmentation de droits, un déni de service ou une
fuite d'informations.</p>

<p>Cette mise à jour n’est pas encore disponible pour l’architecture armhf
(ARM EABI hard-float).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5715">CVE-2017-5715</a>

<p>Plusieurs chercheurs ont découvert une vulnérabilité dans divers
processeurs prenant en charge l'exécution spéculative, permettant à un
attaquant contrôlant un processus non privilégié de lire la mémoire
à partir d'adresses arbitraires, y compris à partir du noyau et de tous les
autres processus exécutés dans le système.</p>

<p>Cette attaque particulière a été nommée Spectre variante 2 (« branch
target injection ») et elle est palliée pour l'architecture Intel x86
(amd64 et i386) en utilisant de nouvelles fonctions dans le microcode.</p>

<p>Cette mitigation nécessite une mise à jour du microcode du processeur qui
n’est pas libre. Pour les processeurs récents d’Intel, cela est inclus dans le
paquet intel-microcode à partir de la version 3.20180425.1~deb8u1. Pour les
autres processeurs, cela pourrait être inclus dans une mise à jour du BIOS du
système ou du micrologiciel UEFI, ou dans une mise à jour future du paquet
amd64-microcode.</p>

<p>Cette vulnérabilité a déjà été mitigée pour l’architecture x86 avec la
fonction <q>retpoline</q>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5753">CVE-2017-5753</a>

<p>De nouvelles instances de code vulnérables à Spectre variante 1
(contournement de vérification de limites) ont été mitigées.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1066">CVE-2018-1066</a>

<p>Dan Aloni a signalé à Red Hat que l’implémentation du client CIFS pourrait
déréférencer un pointeur NULL si le serveur renvoie une réponse non valable lors
du réglage de la négociation NTLMSSP. Cela pourrait être utilisé par un serveur
malveillant pour un déni de service.</p>

<p>La mitigation appliquée précédemment n’était pas appropriée pour Linux 3.16
et a été remplacée par un correctif alternatif.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1093">CVE-2018-1093</a>

<p>Wen Xu a signalé qu’une image de système de fichiers ext4 contrefaite
pourrait déclencher une lecture hors limites dans la fonction
ext4_valid_block_bitmap(). Un utilisateur local capable de monter des systèmes
de fichiers arbitraires pourrait utiliser cela pour un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1130">CVE-2018-1130</a>

<p>Le logiciel syzbot a trouvé que l’implémentation de DCCP de sendmsg() ne
vérifie pas l’état de socket, conduisant éventuellement à un déréférencement de
pointeur NULL. Un utilisateur local pourrait utiliser cela pour provoquer un
déni de service (plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-3665">CVE-2018-3665</a>

<p>Plusieurs chercheurs ont découvert que quelques processeurs x86 d’Intel
peuvent de manière spéculative lire des registres floating-point et vector même
si l’accès à ces registres est désactivé. La fonction du noyau Linux
<q>lazy FPU</q> repose sur ce contrôle d’accès pour éviter l’enregistrement et
la restitution de ces registres pour des tâches ne les utilisant pas, et était
activée par défaut pour les processeurs x86 qui ne prennent pas en charge
l’instruction XSAVEOPT.</p>

<p>Si <q>lazy FPU</q> est activé sur un des processeurs affectés, un attaquant
contrôlant un processus non privilégié peut être capable de lire des
informations sensibles de processus d’autres utilisateurs ou du noyau. Cela
affecte particulièrement les processeurs basés sur les conceptions de cœur
<q>Nehalem</q> et <q>Westemere</q>.

Ce problème a été mitigé en désactivant <q>lazy FPU</q> par défaut sur tous les
processeurs x86 prenant en charge les instructions FXSAVE et FXRSTOR. Cela
inclut tous les processeurs connus comme étant affectés et la plupart des
processeurs réalisant l’exécution spéculative. Il peut être aussi mitigé en
ajoutant le paramètre de noyau : eagerfpu=on.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5814">CVE-2018-5814</a>

<p>Jakub Jirasek a signalé une situation de compétition dans le pilote hôte
USB/IP. Un client malveillant pourrait utiliser cela pour provoquer un déni de
service (plantage ou corruption de mémoire), et éventuellement exécuter du
code, sur le serveur USB/IP.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-9422">CVE-2018-9422</a>

<p>Il a été signalé que l’appel système futex() pourrait être utilisé par
un utilisateur non privilégié pour une augmentation de droits.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10853">CVE-2018-10853</a>

<p>Andy Lutomirski et Mika Penttilä ont signalé que KVM pour les processeurs x86
ne réalisait pas la vérification de privilèges nécessaires lors de de l’émulation
de certaines instructions. Cela pourrait être utilisé par un utilisateur non
privilégié dans une VM de système invité pour augmenter ses droits dans le
système invité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10940">CVE-2018-10940</a>

<p>Dan Carpenter a signalé que le pilote de disque optique (cdrom) ne validait
pas correctement le parametre pour l’ioctl CDROM_MEDIA_CHANGED. Un utilisateur
ayant accès à un périphérique cdrom pourrait utiliser cela pour provoquer un
déni de service (plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-11506">CVE-2018-11506</a>

<p>Piotr Gabriel Kosinski et Daniel Shapira ont signalé que le pilote de disque
optique SCSI (sr) n’allouait pas un tampon suffisamment grand pour les données
indicatives « sense ». Un utilisateur, ayant accès à un périphérique optique SCSI
pouvant produire plus de 64 octets de données indicatives, pourrait utiliser cela
pour provoquer un déni de service (plantage ou corruption de mémoire), et
éventuellement pour une augmentation de droits.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-12233">CVE-2018-12233</a>

<p>Shankara Pailoor a signalé qu’une image de système de fichiers JFS contrefaite
pourrait déclencher un déni de service (corruption de mémoire). Cela pourrait
éventuellement être aussi utilisé pour une augmentation de droits.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000204">CVE-2018-1000204</a>

<p>Le logiciel syzbot a trouvé que le pilote générique SCSI (sg) pouvait en
certaines circonstances permettre de lire des données de tampons non initialisés
qui pourraient inclure des informations sensibles du noyau ou d’autres tâches.
Cependant, seul les utilisateurs privilégiés avec les capacités CAP_SYS_ADMIN ou
CAP_SYS_RAWIO étaient autorisés à cela, donc l’impact de sécurité était faible
ou inexistant.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 3.16.57-1. De plus, cette mise à jour corrige le bogue
Debian n° 898165 et inclut beaucoup d’autres corrections de bogues issues des
mises à jour de la version 3.16.57 stable.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.or/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1422.data"
# $Id: $
