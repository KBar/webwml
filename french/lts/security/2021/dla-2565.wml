#use wml::debian::translation-check translation="343fa0cd30d7d857590aa2cea3df39f1ececae6a" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait deux problèmes dans la branche 1.0 du
système de chiffrement OpenSSL.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-23840">CVE-2021-23840</a>

<p>Les appels à EVP_CipherUpdate, EVP_EncryptUpdate et EVP_DecryptUpdate pouvaient
dépasser l’argument de longueur de sortie dans certains cas où la longueur de
l’entrée est proche de la longueur totale permise pour un entier sur la
plateforme. Dans de tels cas, la valeur de retour de l’appel de fonction
serait 1 (indication de réussite) mais la valeur de longueur de sortie serait
négative. Cela pourrait entraîner un comportement incorrect ou un plantage des
applications. OpenSSL versions 1.1.1i et précédentes sont affectées par ce
problème. Les utilisateurs de telles versions devraient mettre à niveau vers
OpenSSL 1.1.1j. OpenSSL versions 1.0.2x et précédentes sont affectées par ce
problème. Cependant, OpenSSL 1.0.2 n’est plus pris en charge et ne reçoit
plus de mises à jour publiques. Les utilisateurs de la prise en charge Premium
d’OpenSSL 1.0.2 devraient mettre à niveau vers la version 1.0.2y. Les autres
utilisateurs devraient le faire vers la version 1.1.1j. Problème corrigé dans
OpenSSL 1.1.1j (version affectée 1.1.1-1.1.1i). Problème corrigé dans
OpenSSL 1.0.2y (versions affectées 1.0.2-1.0.2x).</p></li>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-23841">CVE-2021-23841</a>

<p>La fonction de l’API publique d’OpenSSL X509_issuer_and_serial_hash() essaie
de créer une valeur de hachage unique basée sur l’émetteur et les données de
numéro de série contenues dans le certificat X509. Cependant elle échoue à gérer
correctement toute erreur survenant lors de l’analyse le champ de l’émetteur
(ce qui peut se produire si le champ de l’émetteur est malveillant). Cela peut
résulter dans un déréférencement de pointeur NULL et un plantage conduisant
à attaque possible par déni de service.</p></li>
</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1.0.2u-1~deb9u4. Pour les modifications équivalentes dans la
branche 1.1, veuillez consulter l’annonce DLA-2563-1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets openssl1.0.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2565.data"
# $Id: $
