#use wml::debian::translation-check translation="c130a9156d94d37ed0d10a6758c99c83c8133287" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Squid, un serveur mandataire de mise en cache de haute performance pour des
clients web, a été découvert vulnérable à des attaques par déni de service
associées avec un traitement d’authentification d’en-têtes HTTP.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12525">CVE-2019-12525</a>

<p>À cause d’une gestion défectueuse de tampon, Squid est vulnérable à une
attaque par déni de service lors du traitement des certificats
d’authentification Digest HTTP.</p>

<p>À cause d’une validation incorrecte d’entrée, l’analyseur d’en-tête HTTP
demandée pour l’authentification Digest peut accéder à la mémoire en dehors de
celle allouée au tampon.</p>

<p>Sur des systèmes avec protections d’accès mémoire cela pourrait aboutir
à ce que le processus Squid se termine de manière imprévue, amenant un déni de
service pour tous les clients utilisant le mandataire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12529">CVE-2019-12529</a>

<p>À cause d’une gestion défectueuse de tampon, Squid est vulnérable à une
attaque par déni de service lors du traitement des certificats
d’authentification Basic HTTP.</p>

<p>À cause d’une terminaison incorrecte de chaîne, le décodeur de certificats
d’authentification Basic peut accéder à la mémoire en dehors du tampon du
décodeur.</p>

<p>Sur des systèmes avec protections d’accès mémoire cela pourrait aboutir
à ce que le processus Squid se termine de manière imprévue, amenant un déni de
service pour tous les clients utilisant le mandataire.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 3.4.8-6+deb8u8.</p>
<p>Nous vous recommandons de mettre à jour vos paquets squid3.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1858.data"
# $Id: $
