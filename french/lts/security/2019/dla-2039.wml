#use wml::debian::translation-check translation="32dcaaef9ca75c0055c09cd8984ba5543ab7f3d3" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Deux problèmes ont été découverts dans libvorbis, a bibliothèque de décodage
pour le codec générique de compression audio Vorbis.</p>

<p>2017-14633</p>

<p>Dans libvorbis 1.3.5 de Xiph.Org, une vulnérabilité de lecture de tableau
hors limites existe dans la fonction mapping0_forward() dans mapping0.c, qui
pourrait conduire à un déni de service lors d’une opération avec
vorbis_analysis() sur un fichier audio contrefait .</p>

<p>2017-11333</p>

<p>La fonction vorbis_analysis_wrote dans lib/block.c dans libvorbis 1.3.5
de Xiph.Org permet à des attaquants distants de provoquer un déni de service
(OOM) à l'aide d'un fichier wav contrefait.</p>


<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.3.4-2+deb8u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libvorbis.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-2039.data"
# $Id: $
