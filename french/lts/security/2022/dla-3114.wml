#use wml::debian::translation-check translation="b7e7e001e33a592d33e4bd140a108100259b2fae" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes ont été découverts dans le serveur de base de
données MariaDB. Ces vulnérabilités sont corrigées en mettant à niveau
MariaDB vers la nouvelle version amont 10.3.36. Veuillez lire les notes de
publication de MariaDB 10.3 pour de plus amples détails :</p>

<p>https://mariadb.com/kb/en/mariadb-10335-release-notes/
<a href="https://mariadb.com/kb/en/mariadb-10336-release-notes/">\
https://mariadb.com/kb/en/mariadb-10336-release-notes/</a>.</p>

<p>Pour Debian 10 Buster, ces problèmes ont été corrigés dans la version
1:10.3.36-0+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets mariadb-10.3.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de mariadb-10.3,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/mariadb-10.3">\
https://security-tracker.debian.org/tracker/mariadb-10.3</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3114.data"
# $Id: $
