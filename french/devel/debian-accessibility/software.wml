#use wml::debian::template title="Debian-Accessibility - Logiciels"
#use wml::debian::translation-check translation="a1eaa8012e6459917b80adb073e753fab27bfdb5" maintainer="Jean-Paul Guillonneau"
{#style#:<link rel="stylesheet" href="style.css" type="text/css" />:#style#}

<define-tag a11y-pkg endtag=required>
<preserve name tag url/>
<set-var %attributes>
<h3><if "<get-var url>"
        <a href="<get-var url>" name="<get-var tag>"><get-var name></a>
      <a href="https://packages.debian.org/<get-var tag>" name="<get-var tag>"><get-var name></a>></h3>
  %body
<restore name tag url/>
</define-tag>


<h2><a id="speech-synthesis" name="speech-synthesis">Synthèse vocale et
    interfaces de programmation d'application apparentées</a></h2>
<p>
  Une liste complète est disponible sur la page consacrée à la
  <a href="https://blends.debian.org/accessibility/tasks/speechsynthesis">tâche
  de synthèse vocale</a>.
</p>

<a11y-pkg name="EFlite" tag=eflite url="http://eflite.sourceforge.net/">
  <p>
  Serveur vocal pour <a href="#emacspeak">Emacspeak</a> et <a href="#yasr">\
  yasr</a> (ou d'autres lecteurs d'écran) qui leur permet de se connecter avec
  <a href="#flite">Festival Lite</a>, un moteur libre de synthèse vocale
  à partir d’un texte, développé au Centre pour la parole de l'Université de
  Carnegie Mellon en tant que ramification de <a href="#festival">Festival</a>.
  </p>
  <p>
  À cause de limitations héritées de son filtre de sortie, EFlite ne fournit de
  prise en charge que pour la langue anglaise pour le moment.
  </p>
</a11y-pkg>

<a11y-pkg name="eSpeak" tag=espeak>
  <p>
  eSpeak/eSpeak-NG est un logiciel de synthèse vocale pour l'anglais et quelques
  autres langues.
  </p>

  <p>
  eSpeak produit un anglais de bonne qualité. Il utilise une méthode de
  synthèse différente des autres moteurs de lecture de texte à code source
  ouvert (pas de synthèse vocale par concaténation, et donc une empreinte
  mémoire très faible), et sonne assez différemment. Il n'est peut-être pas
  aussi naturel ni <q>fluide</q> que d'autres, mais certaines personnes trouvent
  que l'articulation est plus claire et plus facile à écouter sur de longues
  périodes.
  </p>

  <p>
  Il peut également fonctionner en ligne de commande pour énoncer le texte d'un
  fichier ou provenant de l'entrée standard. Il fonctionne aussi en tant
  qu'<q>orateur</q> avec le système de lecture de texte de KDE (KTTS), comme
  alternative à <a href="#festival">Festival</a> par exemple. Ainsi, il peut
  énoncer un texte sélectionné dans le presse-papier, directement du navigateur
  Konqueror ou de l'éditeur Kate.
  </p>

  <ul>
    <li>Il fournit différentes voix dont les caractéristiques peuvent être
      modifiées.</li>
    <li>Il peut produire un fichier WAV en sortie.</li>
    <li>Il peut traduire un texte en codes de phonèmes pour une utilisation
      comme frontal d'un autre moteur de synthèse vocale.</li>
    <li>Il peut être utilisé pour d'autres langues. Des essais rudimentaires (et
      certainement drôles) en allemand et en espéranto sont fournis.</li>
    <li>Il est de taille compacte. Le programme et ses données font environ
      350&nbsp;ko.</li>
    <li>Il est écrit en C++.</li>
  </ul>

  <p>
  eSpeak peut aussi être utilisé avec <a href="#speech-dispatcher">Speech
  Dispatcher</a>.
  </p>
</a11y-pkg>

<a11y-pkg name="Festival Lite" tag=flite>
  <p>
  Petit moteur rapide de synthèse vocale instantanée. Il s'agit du dernier
  ajout à l'ensemble d'outils de synthèse formés de logiciels libres qui
  comprend le système de synthèse vocale Festival de l'Université d’Édimbourg
  et le projet Festvox de l'Université de Carnegie Mellon, des outils, des
  scripts et de la documentation pour construire des voix synthétiques. Quoi
  qu'il en soit, Flite lui-même n'a besoin d'aucun de ces systèmes pour
  fonctionner.
  </p>
  <p>
  Actuellement, il ne gère que la langue anglaise.
  </p>
</a11y-pkg>

<a11y-pkg name="Festival" tag="festival"
          url="http://www.cstr.ed.ac.uk/projects/festival/">
  <p>
  Système de synthèse vocale multilingue universel développé au
  <a href="http://www.cstr.ed.ac.uk/">CSTR</a> (<i>Centre for Speech Technology
  Research</i>, le Centre pour la recherche en technologie de la parole) de
  l'<a href="http://www.ed.ac.uk/text.html">Université d’Édimbourg</a>.
  </p>
  <p>
  Festival propose un système complet de lecture de texte avec diverses
  interfaces de programmation d'applications, ainsi qu'un environnement pour le
  développement et la recherche de techniques de synthèse vocale. Il est écrit
  en C++ avec un interpréteur de commandes basé sur Scheme pour ses commandes
  générales.
  </p>
  <p>
  En plus de la recherche en synthèse vocale, Festival peut être utilisé comme
  programme de synthèse vocale autonome. Il est capable de produire des paroles
  facilement compréhensibles à partir d'un texte.
  </p>
</a11y-pkg>

<a11y-pkg name="Speech Dispatcher" tag="speech-dispatcher"
          url="http://www.freebsoft.org/speechd">
  <p>
  Couche indépendante des périphériques pour la synthèse vocale. Ce projet
  prend en charge comme moteur divers synthétiseurs vocaux logiciels et matériels et
  fournit aux applications une couche générique pour la synthèse de la parole
  ou rejouer des données PCM à l’aide de ces différents moteurs.
  </p>
  <p>
  Divers concepts évolués, comme la mise en file d'attente par opposition
  à l'utilisation d'interruptions pour la sortie de la voix ou des configurations
  particulières d'utilisateur pour des applications, sont mis en &oelig;uvre de façon
  indépendante du périphérique, ce qui évite donc au programmeur d'application
  de devoir à nouveau réinventer la roue.
  </p>
</a11y-pkg>


<h2><a name="i18nspeech">Synthèse vocale internationalisée</a></h2>

<p>
Toutes les solutions libres disponibles actuellement pour la synthèse vocale
basée sur le logiciel semblent partager une déficience commune&nbsp;: elles
sont principalement limitées à l'anglais, ne fournissant qu'une gestion très
marginale pour les autres langues, ou dans la plupart des cas absolument aucune.
Parmi tous les logiciels libres de synthèse vocale pour Linux, seul
Festival, de l’Université de Carnegie Mellon, gère plus d'une seule langue
naturelle. Festival peut synthétiser en anglais, en espagnol et en gallois.
L'allemand, le français et le russe ne sont pas gérés. Alors que la
tendance est à l'internationalisation et la régionalisation des logiciels et des
services sur la Toile, est-il raisonnable de demander aux personnes aveugles
intéressées par Linux d'apprendre l'anglais simplement pour comprendre leur
ordinateur et de faire toute leur correspondance dans une langue
étrangère&nbsp;?
</p>
<p>
Malheureusement, la synthèse vocale n'est pas vraiment le projet maison préféré
de Jeannot le Programmeur. La création d'un logiciel de synthèse vocale
intelligible nécessite des tâches qui prennent beaucoup de temps. La synthèse vocale
par concaténation demande le création soigneuse d'une base de données de
phonèmes contenant toutes les combinaisons possibles de sons de la langue
visée. Les règles pour déterminer la transformation de la représentation
textuelle en phonèmes individuels nécessitent également d'être développées et
très bien ajustées, ce qui demande habituellement la segmentation d'un flux de
caractères en groupes logiques tels que des phrases, des locutions et des mots.
Une telle analyse lexicale nécessite un lexique spécifique à la langue rarement
publié sous une licence libre.
</p>
<p>
L'un des systèmes de synthèse vocale les plus prometteurs est Mbrola, avec une
base de données de phonèmes pour plus de douze langues différentes. La synthèse
elle-même est un logiciel libre. Malheureusement, les bases de données de
phonèmes sont réservées seulement à un usage non militaire et non commercial.
Nous manquons de bases de données libres pour une utilisation dans le système
d’exploitation Debian.
</p>
<p>
Sans un logiciel de synthèse vocale largement multilingue, Linux ne peut pas
être accepté par les fournisseurs de technologies d'assistance et par les
personnes ayant des handicaps visuels. Que pouvons-nous faire pour améliorer
cela&nbsp;?
</p>
<p>
Il y a en fait deux approches possibles&nbsp;:
</p>
<ol>
  <li>
  Organiser un groupe de personnes souhaitant aider dans ce domaine, et essayer
  d'améliorer activement la situation. Ce peut être un peu compliqué, car cela
  nécessite beaucoup de connaissances spécifiques sur la synthèse vocale, ce
  qui n'est pas si simple pour une approche autodidacte. Quoi qu'il en soit, il
  ne faut pas vous décourager. Si vous pensez que vous pouvez motiver un groupe
  de personnes suffisamment grand pour obtenir des améliorations, ça vaut
  vraiment la peine de le faire&nbsp;;
  </li>
  <li>
  Obtenir des fonds et engager un institut qui a déjà le savoir-faire pour
  créer la base de données de phonèmes nécessaire, les lexiques et les règles
  de transformation. Cette approche a l'avantage d'avoir une meilleure
  probabilité de générer des résultats de qualité, et elle devrait également
  apporter certaines améliorations bien plus vite que la première approche. Bien
  sûr, la licence sous laquelle tout le travail résultant devrait être publié
  doit être convenue à l'avance, et elle devrait se conformer aux principes du
  logiciel libre selon Debian. La solution idéale serait bien sûr de convaincre
  une université de suivre un tel projet avec ses propres fonds et de donner le
  résultat à la communauté du logiciel libre.
  </li>
</ol>

<p>
Enfin, il semble que la plupart des produits actuels de synthèse vocale
commerciaux ayant du succès n'utilisent plus la synthèse vocale par
concaténation, principalement car les bases de données de sons consomment
beaucoup d'espace disque. Cela n'est pas souhaitable pour de petits produits
embarqués comme par exemple les téléphones portables. Des logiciels libres
récents comme <a href="#espeak">eSpeak</a> semblent essayer cette approche, ils
valent très certainement la peine d'être regardés.
</p>


<h2><a id="emacs" name="emacs">Extensions de lecture d'écran pour
    Emacs</a></h2>

<a11y-pkg name="Emacspeak" tag="emacspeak"
          url="http://emacspeak.sourceforge.net/">
  <p>
  Système de sortie vocale permettant à quelqu'un qui ne peut pas voir de
  travailler directement sur un système Unix. Une fois que vous avez démarré
  Emacs et chargé Emacspeak, vous recevez un retour vocal de tout ce que vous
  faites. Votre expérience variera en fonction de votre niveau d'utilisation
  d'Emacs. Il n'y a rien qui ne puisse être fait dans Emacs&nbsp;:-). Ce paquet
  comprend des serveurs vocaux écrits en Tcl pour prendre en charge les synthétiseurs
  vocaux DECtalk Express et DECtalk MultiVoice. Pour d'autres synthétiseurs,
  veuillez rechercher un paquet de serveur vocal séparé tel qu'Emacspeak-ss ou
  <a href="#eflite">eflite</a>.
  </p>
</a11y-pkg>

<a11y-pkg name="Speechd-el" tag="speechd-el"
          url="http://www.freebsoft.org/speechd-el">
  <p>
  Client Emacs pour les synthétiseurs vocaux, les afficheurs en Braille et les
  autres interfaces alternatives de sortie. Il fournit un environnement complet
  de sortie avec synthèse vocale et Braille pour Emacs. Il est principalement
  destiné aux utilisateurs déficients visuels qui ont besoin de communication
  non visuelle avec Emacs, mais il peut être utilisé par toute personne
  nécessitant une synthèse vocale ou tout autre type de sortie alternative avec
  Emacs.
  </p>
</a11y-pkg>


<h2><a id="console" name="console">Lecteurs d'écran en console (mode
    texte)</a></h2>

<p>
  Une liste complète est disponible sur la page consacrée à la
  <a href="https://blends.debian.org/accessibility/tasks/console">tâche de lecteurs
d’écran en console</a>.
</p>

<a11y-pkg name="BRLTTY" tag="brltty" url="https://brltty.app/">
  <p>
  Démon fournissant un accès à la console Linux pour une personne aveugle
  utilisant un affichage en braille à cellules souples<!-- soft braille display
  -->. Il pilote le terminal en braille et fournit des fonctionnalités
  complètes de lecture de l'écran.
  </p>
  <p>
  Les périphériques braille gérés par BRLTTY sont listés dans la
  <a href="https://brltty.app/doc/KeyBindings/#braille-device-bindings">
  documentation sur les périphériques BRLTTY</a>.
  </p>
  <p>
  BRLTTY fournit également une infrastructure client/serveur pour
  les applications souhaitant utiliser un afficheur en braille. Le démon écoute
  les connexions TCP/IP entrantes sur un certain port. Une bibliothèque
  d'objets partagés pour les clients est fournie dans le paquet <a
  href="https://packages.debian.org/libbrlapi">libbrlapi</a>. Une bibliothèque
  statique, les fichiers d'en-têtes et la documentation sont fournis dans le
  paquet <a href="https://packages.debian.org/libbrlapi-dev">libbrlapi-dev</a>.
  Cette fonctionnalité est, par exemple, utilisée par <a href="#gnome-orca">\
  Orca</a> pour fournir la prise en charge pour des types d'afficheur qui ne
  sont pas encore gérés par Gnopernicus directement.
  </p>
</a11y-pkg>

<a11y-pkg name="Yasr" tag="yasr" url="http://yasr.sourceforge.net/">
  <p>
  Lecteur d'écran en console universel pour GNU/Linux et d'autres systèmes
  d'exploitation de type Unix. Le nom <q>yasr</q> est un acronyme qui pourrait
  signifier soit <q>Yet Another Screen Reader</q>, encore un autre lecteur
  d'écran, soit <q>Your All-purpose Screen Reader</q>, votre lecteur d'écran
  universel.
  </p>
  <p>
  Actuellement, yasr essaye de prendre en charge les synthétiseurs matériels Speak-out,
  DEC-talk, BNS, Apollo et DoubleTalk. Il est également capable de communiquer
  avec les serveurs vocaux Emacspeak et peut donc être utilisé avec des
  synthétiseurs indirectement gérés, comme <a href="#flite">Festival
  Lite</a> (<i>via</i> <a href="#eflite">eflite</a>) ou FreeTTS.
  </p>
  <p>
  Yasr fonctionne en ouvrant un pseudo-terminal et en lançant un interpréteur
  de commandes, il intercepte toute entrée et toute sortie. Il surveille les
  séquences d'échappement envoyées et entretient une <q>fenêtre</q> virtuelle
  contenant ce qu'il pense être à l'écran. Il n'utilise donc pas de
  fonctionnalités spécifiques à Linux et peut être porté vers un autre système
  d'exploitation de type Unix sans trop de problèmes.
  </p>
</a11y-pkg>


<h2><a id="gui" name="gui">Interface utilisateur graphique</a></h2>

<p>
L'accessibilité des interfaces utilisateur graphiques sur les plates-formes
Unix n'a reçu que récemment des améliorations significatives importantes avec
les différents efforts de développement autour du
<a href="http://www.gnome.org/">bureau GNOME</a>, particulièrement le
<a href="https://wiki.gnome.org/Accessibility">projet d'accessibilité de
GNOME</a>.
</p>

<h2><a id="gnome" name="gnome">Accessibilité de GNOME</a></h2>
<p>
  Une liste complète est disponible sur la page consacrée à la
  <a href="https://blends.debian.org/accessibility/tasks/gnome">tâche
  d’accessibilité de Gnome</a>.
</p>
<a11y-pkg name="Assistive Technology Service Provider Interface" tag="at-spi">
  <p>
  Paquet contenant les composants principaux d'accessibilité de GNOME. Il
  permet aux fournisseurs de technologie d'assistance comme les lecteurs d'écran
  de demander à toutes les applications fonctionnant sur le bureau des
  informations liées à l'accessibilité ainsi que de fournir des mécanismes de
  relais pour prendre en charge des boîtes à outils autres que GTK.
  </p>
  <p>
  Les liaisons pour le langage Python sont fournies dans le paquet
  <a href="https://packages.debian.org/python-at-spi">python-at-spi</a>.
  </p>
</a11y-pkg>

<a11y-pkg name="The ATK accessibility toolkit" tag="atk">
  <p>
  ATK est une boîte à outils fournissant des interfaces d'accessibilité pour
  les applications ou d'autres boîtes à outils. En implémentant ces interfaces,
  ces autres boîtes à outils ou ces applications peuvent être utilisées avec
  des outils tels que des lecteurs d'écran, des loupes et d'autres
  périphériques de saisie alternatifs.
  </p>
  <p>
  Les bibliothèques d'exécution d'ATK, nécessaires au fonctionnement des
  applications construites avec, sont disponibles dans le paquet
  <a href="https://packages.debian.org/libatk1.0-0">libatk1.0-0</a>. Les
  fichiers de développement pour ATK, nécessaires à la compilation des
  programmes ou des boîtes à outils qui l'utilisent, sont fournis dans le
  paquet <a href="https://packages.debian.org/libatk1.0-dev">libatk1.0-dev</a>.
  Les liaisons pour le langage Ruby sont fournies dans le paquet
  <a href="https://packages.debian.org/ruby-atk">libatk1-ruby</a>.
</p>
</a11y-pkg>

<a11y-pkg name="gnome-accessibility-themes" tag="gnome-accessibility-themes">
  <p>
  Le paquet gnome-accessibility-themes contient quelques grands thèmes
  d’accessibilité pour l'environnement de bureau Gnome, conçus pour les
  malvoyants.
  </p>
  <p>
  Un total de sept thèmes est fourni, permettant des combinaisons de contraste
  fort, faible ou inversé, ainsi que des textes et des icônes agrandis.
  </p>
</a11y-pkg>

<a11y-pkg name="gnome-orca" tag="gnome-orca"
          url="http://live.gnome.org/Orca">
  <p>
  Orca est un lecteur flexible et extensible qui permet l'accès au bureau
  graphique par l'intermédiaire de combinaisons personnalisables de la parole,
  du braille et de l'agrandissement de l'affichage. Développé par le bureau des
  programmes d'accessibilité de Sun Microsystems Inc. depuis&nbsp;2004, Orca a
  été créé dès le départ avec le concours et le suivi des utilisateurs auxquels
  il est destiné.
  </p>
  <p>
  Orca peut utiliser <a href="#speech-dispatcher">Speech Dispatcher</a> pour
  fournir une sortie vocale aux utilisateurs. <a href="#brltty">BRLTTY</a> est
  utilisé pour la gestion de l'affichage en braille (ainsi que pour
  l'intégration transparente de la console et de l'interface utilisateur en
  braille).
  </p>
</a11y-pkg>


<h2><a id="kde" name="kde">Logiciels d'accessibilité de KDE</a></h2>
<p>
  Une liste complète est disponible sur la page consacrée à la
  <a href="https://blends.debian.org/accessibility/tasks/kde">tâche
  d’accessibilité de KDE</a>.
 </p>

<a11y-pkg name="kmag" tag="kmag">
  <p>
  Agrandir une partie de l'écran exactement comme si vous utilisiez une loupe
  pour agrandir un journal ou une photographie. Cette application est utile
  pour un grand nombre de personnes&nbsp;: des chercheurs aux artistes, aux
  concepteurs web et aux personnes avec une vision faible.
  </p>
</a11y-pkg>


<h2><a id="input" name="input">Méthodes de saisie inhabituelles</a></h2>
<p>
  Une liste complète est disponible sur la page consacrée aux
  <a href="https://blends.debian.org/accessibility/tasks/input">méthodes de
   saisie</a>.
</p>

<a11y-pkg name="Dasher" tag="dasher" url="http://www.inference.phy.cam.ac.uk/dasher/">
  <p>
  Dasher est une interface de saisie de texte productive, pilotée par des gestes
  naturels de pointage continu. Dasher est un système de saisie de texte
  concurrentiel à chaque fois qu'un clavier complet ne peut pas être utilisé,
  par exemple&nbsp;:
  </p>
  <ul>
    <li>sur un ordinateur portable Palm&nbsp;;</li>
    <li>sur un ordinateur portable&nbsp;;</li>
    <li>lors de l'utilisation d'un ordinateur d'une seule main, avec un manche,
        un écran tactile, une boule roulante ou une souris&nbsp;;</li>
    <li>lors de l'utilisation d'un ordinateur sans les mains (par exemple avec
	un casque-souris<!-- head-mouse --> ou par le suivi du regard<!--
	eyetracker -->).</li>
  </ul>
  <p>
  La version de suivi du regard de Dasher permet à un utilisateur expérimenté
  d'écrire du texte aussi rapidement qu'en écrivant à la main normalement,
  25&nbsp;mots par minutes&nbsp;; en utilisant une souris, un utilisateur
  expérimenté peut écrire 39&nbsp;mots par minute.
  </p>
  <p>
  Dasher utilise un algorithme de prédiction plus avancé que le système T9™
  souvent utilisé dans les téléphones mobiles, le rendant sensible au contexte
  environnant.
  </p>
</a11y-pkg>

<a11y-pkg name="Caribou" tag="caribou" url="https://wiki.gnome.org/Projects/Caribou">
  <p>
  Caribou est une technologie d’assistance de saisie destinée aux utilisateurs
  de dispositif de pointage et cliquage. Il fournit sur l’écran un clavier
  personnalisable avec un mode balayage.
  </p>
</a11y-pkg>
