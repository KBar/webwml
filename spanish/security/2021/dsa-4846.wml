#use wml::debian::translation-check translation="603ec322379c4d19ac2617c6e9f713349947c37e"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se han descubierto varias vulnerabilidades en el navegador web Chromium.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-16044">CVE-2020-16044</a>

    <p>Ned Williamson descubrió un problema de «uso tras liberar» en la implementación
    de WebRTC.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21117">CVE-2021-21117</a>

    <p>Rory McNamara descubrió un problema de imposición de reglas en Cryptohome.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21118">CVE-2021-21118</a>

    <p>Tyler Nighswander descubrió un problema de validación de datos en la biblioteca
    javascript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21119">CVE-2021-21119</a>

    <p>Se descubrió un problema de «uso tras liberar» en el tratamiento de medios.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21120">CVE-2021-21120</a>

    <p>Nan Wang y Guang Gong descubrieron un problema de «uso tras liberar» en la implementación
    de WebSQL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21121">CVE-2021-21121</a>

    <p>Leecraso y Guang Gong descubrieron un problema de «uso tras liberar» en la Omnibox.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21122">CVE-2021-21122</a>

    <p>Renata Hodovan descubrió un problema de «uso tras liberar» en Blink/WebKit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21123">CVE-2021-21123</a>

    <p>Maciej Pulikowski descubrió un problema de validación de datos.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21124">CVE-2021-21124</a>

    <p>Chaoyang Ding descubrió un problema de «uso tras liberar» en el reconocedor de voz.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21125">CVE-2021-21125</a>

    <p>Ron Masas descubrió un problema de imposición de reglas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21126">CVE-2021-21126</a>

    <p>David Erceg descubrió un problema de imposición de reglas en extensiones.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21127">CVE-2021-21127</a>

    <p>Jasminder Pal Singh descubrió un problema de imposición de reglas en extensiones.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21128">CVE-2021-21128</a>

    <p>Liang Dong descubrió un problema de desbordamiento de memoria en Blink/WebKit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21129">CVE-2021-21129</a>

    <p>Maciej Pulikowski descubrió un problema de imposición de reglas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21130">CVE-2021-21130</a>

    <p>Maciej Pulikowski descubrió un problema de imposición de reglas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21131">CVE-2021-21131</a>

    <p>Maciej Pulikowski descubrió un problema de imposición de reglas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21132">CVE-2021-21132</a>

    <p>David Erceg descubrió un error de implementación en las herramientas para programadores.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21133">CVE-2021-21133</a>

    <p>wester0x01 descubrió un problema de imposición de reglas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21134">CVE-2021-21134</a>

    <p>wester0x01 descubrió un error en la interfaz de usuario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21135">CVE-2021-21135</a>

    <p>ndevtk descubrió un error de implementación en la API de rendimiento («Performance API»).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21136">CVE-2021-21136</a>

    <p>Shiv Sahni, Movnavinothan V e Imdad Mohammed descubrieron un problema
    de imposición de reglas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21137">CVE-2021-21137</a>

    <p>bobbybear descubrió un error de implementación en las herramientas para programadores.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21138">CVE-2021-21138</a>

    <p>Weipeng Jiang descubrió un problema de «uso tras liberar» en las herramientas para programadores.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21139">CVE-2021-21139</a>

    <p>Jun Kokatsu descubrió un error de implementación en el entorno aislado («sandbox») para iframes.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21140">CVE-2021-21140</a>

    <p>David Manouchehri descubrió memoria no inicializada en la implementación
    de USB.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21141">CVE-2021-21141</a>

    <p>Maciej Pulikowski descubrió un problema de imposición de reglas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21142">CVE-2021-21142</a>

    <p>Khalil Zhani descubrió un problema de «uso tras liberar».</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21143">CVE-2021-21143</a>

    <p>Allen Parker y Alex Morgan descubrieron un problema de desbordamiento de memoria en
    extensiones.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21144">CVE-2021-21144</a>

    <p>Leecraso y Guang Gong descubrieron un problema de desbordamiento de memoria.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21145">CVE-2021-21145</a>

    <p>Se descubrió un problema de «uso tras liberar».</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21146">CVE-2021-21146</a>

    <p>Alison Huffman y Choongwoo Han descubrieron un problema de «uso tras liberar».</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21147">CVE-2021-21147</a>

    <p>Roman Starkov descubrió un error de implementación en la biblioteca skia.</p></li>

</ul>

<p>Para la distribución «estable» (buster), estos problemas se han corregido en
la versión 88.0.4324.146-1~deb10u1.</p>

<p>Le recomendamos que actualice los paquetes de chromium.</p>

<p>Para información detallada sobre el estado de seguridad de chromium, consulte
su página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/chromium">\
https://security-tracker.debian.org/tracker/chromium</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4846.data"
