#use wml::debian::translation-check translation="a898fc728e075d68b4098dd43596590bc9b7a3ef"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se han descubierto varias vulnerabilidades en cURL, una biblioteca para transferencia
de URL. El proyecto «Vulnerabilidades y exposiciones comunes» («Common Vulnerabilities and Exposures») identifica los
problemas siguientes:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-1000100">CVE-2017-1000100</a>

    <p>Even Rouault informó de que cURL no gestiona correctamente los nombres de fichero
    largos al hacer subidas TFTP. Un servidor HTTP(S) malicioso puede aprovechar
    este defecto redireccionando a un cliente que esté utilizando la biblioteca
    cURL a una URL de TFTP manipulada y haciéndole enviar contenidos de la memoria
    privada a un servidor remoto por UDP.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-1000101">CVE-2017-1000101</a>

    <p>Brian Carpenter y Yongji Ouyang informaron de que cURL contiene un defecto
    en la función para expansión de expresiones que contienen caracteres comodín («globbing function») que analiza sintácticamente el rango numérico, dando lugar a
    una lectura fuera de límites al analizar sintácticamente una URL preparada de una manera determinada.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-1000254">CVE-2017-1000254</a>

    <p>Max Dymond informó de que cURL contiene un defecto de lectura fuera de límites en
    el analizador sintáctico de la respuesta PWD de FTP. Un servidor malicioso puede aprovechar
    este defecto para impedir que un cliente que utilice la biblioteca cURL
    opere con él, provocando denegación de servicio.</p></li>

</ul>

<p>Para la distribución «antigua estable» (jessie), estos problemas se han corregido
en la versión 7.38.0-4+deb8u6.</p>

<p>Para la distribución «estable» (stretch), estos problemas se han corregido en
la versión 7.52.1-5+deb9u1.</p>

<p>Le recomendamos que actualice los paquetes de curl.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2017/dsa-3992.data"
