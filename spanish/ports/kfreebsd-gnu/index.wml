#use wml::debian::template title="Debian GNU/kFreeBSD"
#use wml::debian::translation-check translation="6f3adf6374f35194686f89dec2ba66b1ecf3bb5f" maintainer="Diddier Hilarion"
#use wml::debian::toc

<toc-display/>

<p>Debian GNU/kFreeBSD es una adaptación que consta de
<a href="https://www.gnu.org/">las aplicaciones de GNU en espacio de usuario</a> que
usan <a href="https://www.gnu.org/software/libc/">la biblioteca de C de GNU</a> ejecutándose
sobre el núcleo <a href="https://www.freebsd.org/">FreeBSD</a>, todo esto con el
conjunto regular de <a href="https://packages.debian.org/">paquetes de Debian</a>.</p>

<div class="important">
<p>Debian GNU/kFreeBSD es una arquitectura que no está soportada oficialmente. Ha sido publicada
con Debian 6.0 (Squeeze) y 7.0 (Wheezy) como un <em>avance tecnológico</em> y la primera adaptación
no Linux. Desde Debian 8 (Jessie) ya no está incluida en las publicaciones oficiales.</p>
</div>

<toc-add-entry name="resources">Recursos</toc-add-entry>

<p>Hay más información acerca esta adaptación (incluyendo una FAQ) en la página de la wiki
<a href="https://wiki.debian.org/Debian_GNU/kFreeBSD">Debian GNU/kFreeBSD</a>.
</p>

<h3>Listas de correo</h3>
<p><a href="https://lists.debian.org/debian-bsd">Lista de correo Debian GNU/k*BSD</a>.</p>
<h3>IRC</h3>
<p><a href="irc://irc.debian.org/#debian-kbsd">En el canal #debian-kbsd</a> (en irc.debian.org).</p>

<toc-add-entry name="Development">Desarrollo.</toc-add-entry>

<p>Debido a que usamos Glibc los problemas de portabilidad son bastante simples y la mayoría de las veces
basta con copiar un caso de prueba para "k*bsd*-gnu" de otro sistema basado en Glibc
(como GNU o GNU/Linux).  Visite el documento que se refiere a cómo
<a href="https://salsa.debian.org/bsd-team/web/raw/master/porting/PORTING">adaptar</a>
para obtener detalles.</p>

<p>También vea el fichero <a href="https://salsa.debian.org/bsd-team/web/raw/master/TODO">TODO (por hacer)</a>
para más información acerca de qué necesita hacerse.</p>

<toc-add-entry name="availablehw">Hardware disponible para desarrolladores de Debian</toc-add-entry>

<p>Los desarrolladores de Debian tienen disponible lemon.debian.net (kfreebsd-amd64) para labores de adaptación. 
Por favor visite la <a href="https://db.debian.org/machines.cgi">base de datos de máquinas</a> para más
información acerca de estas máquinas. En general, usted podrá usar los dos entornos chroot: testing y unstable. 
Observe que estos sistemas no son administrados por el DSA (Equipo de administradores de sistemas de Debian), 
así que <b>no envíe solicitudes a debian-admin para estos</b>. En vez de esto envíelas a <email "admin@lemon.debian.net">.</p>
