<define-tag pagetitle>Debian Installer Bullseye Alpha 2 release</define-tag>
<define-tag release_date>2020-03-16</define-tag>
#use wml::debian::news

<p>
The Debian Installer <a
href="https://wiki.debian.org/DebianInstaller/Team">team</a> is pleased to
announce the second alpha release of the installer for Debian 11
<q>Bullseye</q>.
</p>


<h2>Improvements in this release</h2>

<ul>
  <li>anna:
    <ul>
      <li>Turn warning regarding missing kernel modules into a more
        descriptive error (<a href="https://bugs.debian.org/749991">#749991</a>, <a href="https://bugs.debian.org/367515">#367515</a>).</li>
    </ul>
  </li>
  <li>clock-setup:
    <ul>
      <li>Rephrase template about the benefit of a correctly set up
        clock.</li>
    </ul>
  </li>
  <li>espeakup:
    <ul>
      <li>Do not use sleep with subsecond durations.</li>
      <li>Print the number of detected cards.</li>
    </ul>
  </li>
  <li>debian-installer:
    <ul>
      <li>Make write-built-using more robust.</li>
      <li>Bump Linux kernel ABI to 5.4.0-4.</li>
    </ul>
  </li>
  <li>glibc:
    <ul>
      <li>Make libc-udeb depends on libcrypt1-udeb (<a href="https://bugs.debian.org/941853">#941853</a>).</li>
    </ul>
  </li>
  <li>grub-installer:
    <ul>
      <li>Change template, to clarify definition of the just installed
        Debian system.</li>
    </ul>
  </li>
  <li>netcfg:
    <ul>
      <li>Improve templates (especially regarding malformed IP
        addresses).</li>
    </ul>
  </li>
  <li>pkgsel:
    <ul>
      <li>Ensure tasksel is installed, regardless of its
        priority.</li>
      <li>Add preseedable <code>pkgsel/run_tasksel</code> debconf template, making
        it possible (by setting it to <code>false</code>) to skip tasksel entirely
        (installation and prompt), while still benefiting from other
        pkgsel features.</li>
    </ul>
  </li>
  <li>preseed:
    <ul>
      <li>Update <code>auto-install/defaultroot</code>, replacing buster with
        bullseye.</li>
    </ul>
  </li>
  <li>rootskel:
    <ul>
      <li>Re-order script to make sure users are set up.</li>
      <li>Enable high-contrast theme when installation was made with
        the dark theme.</li>
      <li>Enable compiz ezoom features for accessibility.</li>
      <li>Specify fs_spec field in fstab-linux mount points. Some
        tools parse /proc/mounts and try to match on the spec field to
        find where sysfs is mounted (ie. dasdfmt on s390x). This makes
        the installer environment look more similar to installed
        systems.</li>
      <li>Tweak how multiple consoles are used. If preseeding is
        detected, do not run on multiple consoles in parallel as that
        causes race conditions and weird behaviour. Instead, just run
        on the "preferred" console (<a href="https://bugs.debian.org/940028">#940028</a>, <a href="https://bugs.debian.org/932416">#932416</a>).</li>
    </ul>
  </li>
  <li>systemd:
    <ul>
      <li>Use 73-usb-net-by-mac.link in udev-udeb (See also:
        <a href="https://bugs.debian.org/946196">#946196</a>).</li>
    </ul>
  </li>
  <li>user-setup:
    <ul>
      <li>Add input, kvm, and render to reserved-usernames; udev.postinst
        adds these as system groups.</li>
    </ul>
  </li>
</ul>


<h2>Hardware support changes</h2>

<ul>
  <li>debian-installer:
    <ul>
      <li>Switch from vmlinux to vmlinuz for mips* except on Octeon.</li>
      <li>Update Firefly-RK3288 image for new u-boot version.</li>
    </ul>
  </li>
  <li>flash-kernel:
    <ul>
      <li>Add support for bootspec entries, bootable by barebox
        (<a href="https://bugs.debian.org/931953">#931953</a>).</li>
      <li>Fix a corner case that makes flash-kernel fail silently
        (<a href="https://bugs.debian.org/932231">#932231</a>).</li>
      <li>Make initrd optional for machines with Boot-Multi-Path
        (<a href="https://bugs.debian.org/869073">#869073</a>).</li>
      <li>Add support for Librem 5 devkit (<a href="https://bugs.debian.org/927700">#927700</a>).</li>
      <li>Add support for OLPC XO-1.75 laptops.</li>
    </ul>
  </li>
</ul>


<h2>Localization status</h2>

<ul>
  <li>76 languages are supported in this release.</li>
  <li>Full translation for 12 of them.</li>
</ul>


<h2>Known issues in this release</h2>

<p>
See the <a href="$(DEVEL)/debian-installer/errata">errata</a> for
details and a full list of known issues.
</p>


<h2>Feedback for this release</h2>

<p>
We need your help to find bugs and further improve the installer, so please
try it. Installer CDs, other media and everything else you will need are
available at our <a href="$(DEVEL)/debian-installer">web site</a>.
</p>


<h2>Thanks</h2>

<p>
The Debian Installer team thanks everybody who has contributed to this
release.
</p>
