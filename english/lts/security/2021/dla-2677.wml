<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security issues have been discovered in libwebp</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-25009">CVE-2018-25009</a>

     <p>An out-of-bounds read was found in function WebPMuxCreateInternal.
     The highest threat from this vulnerability is to data confidentiality
     and to the service availability.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-25010">CVE-2018-25010</a>

    <p>An out-of-bounds read was found in function ApplyFilter.
    The highest threat from this vulnerability is to data confidentiality
    and to the service availability.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-25011">CVE-2018-25011</a>

    <p>A heap-based buffer overflow was found in PutLE16().
    The highest threat from this vulnerability is to data confidentiality
    and integrity as well as system availability.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-25012">CVE-2018-25012</a>

    <p>An out-of-bounds read was found in function WebPMuxCreateInternal.
    The highest threat from this vulnerability is to data confidentiality
    and to the service availability.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-25013">CVE-2018-25013</a>

    <p>An out-of-bounds read was found in function ShiftBytes.
    The highest threat from this vulnerability is to data confidentiality
    and to the service availability.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-25014">CVE-2018-25014</a>

    <p>An unitialized variable is used in function ReadSymbol.
    The highest threat from this vulnerability is to data confidentiality
    and integrity as well as system availability.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36328">CVE-2020-36328</a>

    <p>A heap-based buffer overflow in function WebPDecodeRGBInto is possible
    due to an invalid check for buffer size. The highest threat from this
    vulnerability is to data confidentiality and integrity as well as system
    availability.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36329">CVE-2020-36329</a>

    <p>A use-after-free was found due to a thread being killed too early.
    The highest threat from this vulnerability is to data confidentiality
    and integrity as well as system availability.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36330">CVE-2020-36330</a>

    <p>An out-of-bounds read was found in function ChunkVerifyAndAssign.
    The highest threat from this vulnerability is to data confidentiality
    and to the service availability.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36331">CVE-2020-36331</a>

    <p>An out-of-bounds read was found in function ChunkAssignData.
    The highest threat from this vulnerability is to data confidentiality
    and to the service availability.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
0.5.2-1+deb9u1.</p>

<p>We recommend that you upgrade your libwebp packages.</p>

<p>For the detailed security status of libwebp please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libwebp">https://security-tracker.debian.org/tracker/libwebp</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2677.data"
# $Id: $
