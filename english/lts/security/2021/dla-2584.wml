<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A buffer overflow issue in caca_resize function in
libcaca/caca/canvas.c may lead to local execution of arbitrary code in
the user context.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
0.99.beta19-2.1~deb9u2.</p>

<p>We recommend that you upgrade your libcaca packages.</p>

<p>For the detailed security status of libcaca please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libcaca">https://security-tracker.debian.org/tracker/libcaca</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2584.data"
# $Id: $
