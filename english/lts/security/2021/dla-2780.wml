<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilites in ruby2.3, interpreter of object-oriented
scripting language Ruby, were discovered.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-31799">CVE-2021-31799</a>

    <p>In RDoc 3.11 through 6.x before 6.3.1, as distributed with
    Ruby through 2.3.3, it is possible to execute arbitrary
    code via | and tags in a filename.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-31810">CVE-2021-31810</a>

    <p>An issue was discovered in Ruby through 2.3.3. A malicious
    FTP server can use the PASV response to trick Net::FTP into
    connecting back to a given IP address and port. This
    potentially makes curl extract information about services
    that are otherwise private and not disclosed (e.g., the
    attacker can conduct port scans and service banner extractions).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-32066">CVE-2021-32066</a>

    <p>An issue was discovered in Ruby through 2.3.3. Net::IMAP does
    not raise an exception when StartTLS fails with an an unknown
    response, which might allow man-in-the-middle attackers to
    bypass the TLS protections by leveraging a network position
    between the client and the registry to block the StartTLS
    command, aka a "StartTLS stripping attack."</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
2.3.3-1+deb9u10.</p>

<p>We recommend that you upgrade your ruby2.3 packages.</p>

<p>For the detailed security status of ruby2.3 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/ruby2.3">https://security-tracker.debian.org/tracker/ruby2.3</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2780.data"
# $Id: $
