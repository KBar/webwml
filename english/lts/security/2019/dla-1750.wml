<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a cross-site scripting (XSS) vulnerability
in the web front-end of the roundup tracking system.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10904">CVE-2019-10904</a>

    <p>Roundup 1.6 allows XSS via the URI because frontends/roundup.cgi and roundup/cgi/wsgi_handler.py mishandle 404 errors.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.4.20-1.1+deb8u2.</p>

<p>We recommend that you upgrade your roundup packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1750.data"
# $Id: $
