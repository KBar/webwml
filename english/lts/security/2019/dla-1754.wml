<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Various vulnerabilities were discovered in Samba, SMB/CIFS file, print,
and login server/client for Unix</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9461">CVE-2017-9461</a>

    <p>smbd in Samba had a denial of service vulnerability (fd_open_atomic
    infinite loop with high CPU usage and memory consumption) due to
    wrongly handling dangling symlinks.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1050">CVE-2018-1050</a>

    <p>Samba was vulnerable to a denial of service attack when the RPC
    spoolss service was configured to be run as an external daemon.
    Missing input sanitization checks on some of the input parameters to
    spoolss RPC calls could have caused the print spooler service to
    crash.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1057">CVE-2018-1057</a>

    <p>On a Samba 4 AD DC the LDAP server of Samba incorrectly validated
    permissions to modify passwords over LDAP allowing authenticated
    users to change any other users' passwords, including administrative
    users and privileged service accounts (eg Domain Controllers).</p>

    <p>Thanks to the Ubuntu security team for having backported the rather
    invasive changeset to Samba in Ubuntu 14.04 (which we could use to
    patch Samba in Debian jessie LTS).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3880">CVE-2019-3880</a>

    <p>A flaw was found in the way Samba implemented an RPC endpoint
    emulating the Windows registry service API. An unprivileged attacker
    could have used this flaw to create a new registry hive file anywhere
    they had unix permissions which could have lead to creation of a new
    file in the Samba share.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2:4.2.14+dfsg-0+deb8u12.</p>

<p>We recommend that you upgrade your samba packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1754.data"
# $Id: $
