<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that libtirpc, a transport-independent RPC library,
does not properly handle idle TCP connections. A remote attacker can
take advantage of this flaw to cause a denial of service.</p>

<p>For Debian 10 buster, this problem has been fixed in version
1.1.4-0.4+deb10u1.</p>

<p>We recommend that you upgrade your libtirpc packages.</p>

<p>For the detailed security status of libtirpc please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libtirpc">https://security-tracker.debian.org/tracker/libtirpc</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3071.data"
# $Id: $
