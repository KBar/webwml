<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Tavis Ormandy discovered that the BN_mod_sqrt() function of OpenSSL
could be tricked into an infinite loop. This could result in denial of
service via malformed certificates.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.0.2u-1~deb9u7.</p>

<p>We recommend that you upgrade your openssl1.0 packages.</p>

<p>For the detailed security status of openssl1.0 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/openssl1.0">https://security-tracker.debian.org/tracker/openssl1.0</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2953.data"
# $Id: $
