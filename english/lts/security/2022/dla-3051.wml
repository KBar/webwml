<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>This update includes the latest changes to the leap second list,
including an update to its expiry date, which was set for the end of
June.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
2021a-0+deb9u4.</p>

<p>We recommend that you upgrade your tzdata packages.</p>

<p>For the detailed security status of tzdata please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/tzdata">https://security-tracker.debian.org/tracker/tzdata</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3051.data"
# $Id: $
