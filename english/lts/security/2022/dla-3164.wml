<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Multiple vulnerabilities were discovered in Django, a popular Python-based
web development framework:</p>

<ul>
  <li><a href="https://security-tracker.debian.org/tracker/CVE-2020-24583">CVE-2020-24583</a>:
  Fix incorrect permissions on intermediate-level directories on Python 3.7+.
  FILE_UPLOAD_DIRECTORY_PERMISSIONS mode was not applied to intermediate-level
  directories created in the process of uploading files and to
  intermediate-level collected static directories when using the collectstatic
  management command. You should review and manually fix permissions on
  existing intermediate-level directories.</li>

  <li><a href="https://security-tracker.debian.org/tracker/CVE-2020-24584">CVE-2020-24584</a>:
  Correct permission escalation vulnerability in intermediate-level directories
  of the file system cache. On Python 3.7 and above, the intermediate-level
  directories of the file system cache had the system's standard umask rather
  than 0o077 (no group or others permissions).</li>

  <li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3281l">CVE-2021-3281</a>:
  Fix a potential directory-traversal exploit via archive.extract(). The
  django.utils.archive.extract() function, used by startapp --template and
  startproject --template, allowed directory traversal via an archive with
  absolute paths or relative paths with dot segments.</li>

  <li><a href="https://security-tracker.debian.org/tracker/CVE-2021-23336">CVE-2021-23336</a>:
  Prevent a web cache poisoning attack via "parameter cloaking". Django
  contains a copy of urllib.parse.parse_qsl() which was added to backport some
  security fixes. A further security fix has been issued recently such that
  parse_qsl() no longer allows using ";" as a query parameter separator by
  default.</li>

  <li><a href="https://security-tracker.debian.org/tracker/CVE-2022-34265">CVE-2022-34265</a>:
  The Trunc() and Extract() database functions were subject to a potential SQL
  injection attach if untrusted data was used as a value for the "kind" or
  "lookup_name" parameters. Applications that constrain the choice to a known
  safe list were unaffected.</li>
</ul>

<ul>

<p>For Debian 10 <q>Buster</q>, these problems have been fixed in version
1:1.11.29-1+deb10u2.</p>

<p>We recommend that you upgrade your python-django packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3164.data"
# $Id: $
