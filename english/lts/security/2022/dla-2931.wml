<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that the SQL plugin in cyrus-sasl2, a library
implementing the Simple Authentication and Security Layer, is prone to a
SQL injection attack. An authenticated remote attacker can take advantage
of this flaw to execute arbitrary SQL commands and for privilege
escalation.</p>


<p>For Debian 9 stretch, this problem has been fixed in version
2.1.27~101-g0780600+dfsg-3+deb9u2.</p>

<p>We recommend that you upgrade your cyrus-sasl2 packages.</p>

<p>For the detailed security status of cyrus-sasl2 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/cyrus-sasl2">https://security-tracker.debian.org/tracker/cyrus-sasl2</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2931.data"
# $Id: $
