<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Version 4.0.2-6+deb7u7 introduced changes that resulted in libtiff
being unable to write out tiff files when the compression scheme
in use relies on codec-specific TIFF tags embedded in the image.</p>

<p>This problem manifested itself with errors like those:<br>
$ tiffcp -r 16 -c jpeg sample.tif out.tif<br>
_TIFFVGetField: out.tif: Invalid tag <q>Predictor</q> (not supported by codec).<br>
_TIFFVGetField: out.tif: Invalid tag <q>BadFaxLines</q> (not supported by codec).<br>
tiffcp: tif_dirwrite.c:687: TIFFWriteDirectorySec: Assertion `0' failed.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
4.0.2-6+deb7u10.</p>

<p>We recommend that you upgrade your tiff packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-693-2.data"
# $Id: $
