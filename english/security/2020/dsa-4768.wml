<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Multiple security issues have been found in the Mozilla Firefox web
browser, which could potentially result in the execution of arbitrary
code, cross-site scripting or spoofing the origin of a download.</p>

<p>Debian follows the extended support releases (ESR) of Firefox. Support
for the 68.x series has ended, so starting with this update we're now
following the 78.x releases.</p>

<p>Between 68.x and 78.x, Firefox has seen a number of feature updates.
For more information please refer to
<a href="https://www.mozilla.org/en-US/firefox/78.0esr/releasenotes/">\
https://www.mozilla.org/en-US/firefox/78.0esr/releasenotes/</a></p>

<p>For the stable distribution (buster), these problems have been fixed in
version 78.3.0esr-1~deb10u1.</p>

<p>We recommend that you upgrade your firefox-esr packages.</p>

<p>For the detailed security status of firefox-esr please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/firefox-esr">\
https://security-tracker.debian.org/tracker/firefox-esr</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4768.data"
# $Id: $
