<define-tag description>security update</define-tag>
<define-tag moreinfo>

<p>It was found that the fix to address <a
href="https://security-tracker.debian.org/tracker/CVE-2021-44228">\
CVE-2021-44228</a> in Apache Log4j, a Logging Framework for Java, was
incomplete in certain non-default configurations. This could allow
attackers with control over Thread Context Map (MDC) input data when
the logging configuration uses a non-default Pattern Layout with
either a Context Lookup (for example, $${ctx:loginId}) or a Thread
Context Map pattern (%X, %mdc, or %MDC) to craft malicious input data
using a JNDI Lookup pattern resulting in a denial of service (DOS)
attack.</p>

<p>For the oldstable distribution (buster), this problem has been fixed
in version 2.16.0-1~deb10u1.</p>

<p>For the stable distribution (bullseye), this problem has been fixed in
version 2.16.0-1~deb11u1.</p>

<p>We recommend that you upgrade your apache-log4j2 packages.</p>

<p>For the detailed security status of apache-log4j2 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/apache-log4j2">\
https://security-tracker.debian.org/tracker/apache-log4j2</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-5022.data"
# $Id: $
