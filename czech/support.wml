#use wml::debian::template title="Podpora"
#use wml::debian::toc
#use wml::debian::translation-check translation="76ce7e7f5cfd0e2f3e8931269bdcd1f9518ee67f" maintainer="Michal Simunek"

<define-tag toc-title-formatting endtag="required" whitespace="delete">
<h2>%body</h2>
</define-tag>

Debian a jeho podpora je provozována komunitou dobrovolníků.

Pokud tato komunitní podpora nevyhovuje vašim potřebám, můžete 
si přečíst <a href="doc/">dokumentace</a> či
najmout si <a href="consultants/">konzultanta</a>.

<toc-display />

<toc-add-entry name="irc">On-line nápověda v&nbsp;reálném čase za použití IRC</toc-add-entry>

<p><a href="http://www.irchelp.org/">IRC (Internet Relay Chat,
internetová služba pro on-line komunikaci)</a> je způsob, jak
v&nbsp;reálném čase hovořit s&nbsp;lidmi z&nbsp;celého světa. IRC
kanály vyhrazené Debianu můžete najít
na <a href="https://www.oftc.net/">OFTC</a>.</p>

<p>Pro připojení budete potřebovat nějakého IRC klienta. K&nbsp;nejpopulárnějším klientům patří
<a href="https://packages.debian.org/stable/net/hexchat">HexChat</a>,
<a href="https://packages.debian.org/stable/net/ircii">ircII</a>,
<a href="https://packages.debian.org/stable/net/irssi">irssi</a>,
<a href="https://packages.debian.org/stable/net/epic5">epic5</a> a
<a href="https://packages.debian.org/stable/net/kvirc">KVIrc</a>,
kteří jsou všichni součástí distribuce Debian. OFTC také nabízí <a href="https://www.oftc.net/WebChat/">WebChat</a>
webové rozhraní, které vám umožní připojit se k IRC pomocí vašeho prohlížeče
bez nutnosti instalovat lokálního klienta.</p>

<p>Když máte klienta nainstalovaného, potřebujete mu říci, aby se připojil
k&nbsp;serveru. Ve většině klientů to lze provést napsáním:</p>

<pre>
/server irc.debian.org
</pre>

<p>U některých klientů (například irssi), bude zapotřebí místo toho napsat:</p>

<pre>
/connect irc.debian.org
</pre>

# Note to translators:
# You might want to insert here a paragraph stating which IRC channel is available
# for user support in your language and pointing to the English IRC channel.
# <p>Once you are connected, join channel <code>#debian-foo</code> by typing</p>
# <pre>/join #debian</pre>
# for support in your language.
# <p>For support in English, read on</p>

<p>Jakmile jste připojeni, vstoupíte do kanálu <code>#debian</code>
napsáním</p>

<pre>
/join #debian
</pre>

<p>Poznámka: klienti jako HexChat mají pro připojení na servery/kanály
často různé grafické průvodce.</p>

<p>V&nbsp;tomto okamžiku se objevíte v&nbsp;přátelském davu
obyvatelů kanálu <code>#debian</code>. Máte zde možnost ptát se na otázky
kolem Debianu. Odpovědi na nejčastější otázky v&nbsp;kanálu si můžete
prohlédnout na adrese <url "https://wiki.debian.org/DebianIRC" />.</p>


<p>Existuje několik dalších IRC sítí, kde také můžete diskutovat
o&nbsp;Debianu.
</p>


<toc-add-entry name="mail_lists" href="MailingLists/">Poštovní
konference</toc-add-entry>

<p>Debian je vyvíjen distribuovaně po celém světě. Proto je
e-mail upřednostňovaný způsob diskuze různých témat. Většina rozhovorů
mezi vývojáři Debianu a&nbsp;uživateli je vedena v&nbsp;několika
poštovních konferencích.</p>

<p>Většina poštovních konferencí je veřejně přístupná. Pro více
informací se podívejte na stránku <a href="MailingLists/">poštovních
konferencí Debianu</a>.</p>

# Note to translators:
# You might want to adapt the following paragraph, stating which list
# is available for user support in your language instead of English.
<p>
Pro uživatelskou podporu v angličtině se prosím obraťte na 
<a href="https://lists.debian.org/debian-user/">poštovní konferenci
debian-user</a>.
</p>

<p>
Pro uživatelskou podporu v ostatních jazycích se prosím podívejte na
<a href="https://lists.debian.org/users.html">přehled poštovních
konferencí pro uživatele</a>.
</p>

<p>Kromě toho existuje spousta dalších poštovních konferencí
týkajících se obsáhlého ekosystému Linuxu, které však nejsou specifické pro
Debian. Použijte váš oblíbený vyhledávač a najděte tu, která vám bude vyhovovat nejlépe.</p>


<toc-add-entry name="usenet">Diskuzní skupiny Usenet</toc-add-entry>

<p>Mnoho našich <a href="#mail_lists">poštovních konferencí</a> může
být prohlíženo formou diskuzních skupin v&nbsp;hierarchii
<kbd>linux.debian.*</kbd>. Ke skupinám se dostanete také pomocí
webového rozhraní, jako jsou například <a
href="https://groups.google.com/forum/">Google Groups</a>.</p>


<toc-add-entry name="web">Webové stránky</toc-add-entry>

<h3 id="forums">Fóra</h3>

# Note to translators:
# If there is a specific Debian forum for your language you might want to
# insert here a paragraph stating which list is available for user support
# in your language and pointing to the English forums.
# <p><a href="http://someforum.example.org/">someforum</a> is a web portal
# on which you can use your language to discuss Debian-related topics,
# submit questions about Debian, and have them answered by other users.</p>
#
# <p><a href="https://forums.debian.net">Debian User Forums</a> is a web portal
# on which you can use the English language to discuss Debian-related topics,
# submit questions about Debian, and have them answered by other users.</p>

<p><a href="https://forums.debian.net">Uživatelské fórum Debianu</a> je webový portál, na kterém můžete probírat témata vztahující se k Debianu, pokládat otázky o Debianu
a dostávat odpovědi od ostatních uživatelů.</p>


<toc-add-entry name="maintainers">Komunikace se správci
balíčků</toc-add-entry>

<p>Existují dva způsoby jak oslovit správce balíčků. Pokud potřebujete
kontaktovat správce z&nbsp;důvodu chyby, jednoduše podejte zprávu
o&nbsp;chybě (viz níže kapitola o&nbsp;systému sledování
chyb). Správce dostane kopii zprávy o&nbsp;chybě.</p>

<p>Pokud chcete se správcem prostě komunikovat, můžete použít
speciální emailovou adresu založenou pro každý balíček. Jakýkoliv
email poslaný na adresu &lt;<em>jméno
balíčku</em>&gt;@packages.debian.org bude přeposlán správci, který je
za tento balíček zodpovědný.</p>


<toc-add-entry name="bts" href="Bugs/">Systém sledování chyb</toc-add-entry>

<p>Distribuce Debian má systém sledování chyb, který uchovává
chyby hlášené uživateli a&nbsp;vývojáři. Každá chyba je označena
číslem a&nbsp;dokud není označena za vyřešenou, zůstává v&nbsp;evidenci
otevřených chyb.</p>

<p>K&nbsp;ohlášení chyby můžete použít jednu ze stránek uvedených níže
nebo použít Debianí balíček <q>reportbug</q> pro automatické podání
zprávy o&nbsp;chybě.</p>

<p>Informace o&nbsp;hlášení chyb, prohlížení aktuálních chyb
a&nbsp;samotném systému sledování chyb naleznete na <a
href="Bugs/">stránkách systému sledování chyb</a>.</p>


<toc-add-entry name="consultants" href="consultants/">Konzultanti</toc-add-entry>

<p>Debian je svobodný software a&nbsp;poskytuje bezplatnou nápovědu
pomocí emailových konferencí. Někteří lidé buď nemají čas nebo mají
speciální potřeby a&nbsp;jsou ochotni najmout někoho na správu nebo
přidání dodatečné funkčnosti do svého systému
s&nbsp;Debianem. Podívejte se na <a href="consultants/">stránku
s&nbsp;konzultanty</a>, kde je seznam lidí/společností.</p>


<toc-add-entry name="release" href="releases/stable/">Známé
problémy</toc-add-entry>

<p>Omezení a&nbsp;závažné problémy aktuální stabilní distribuce (pokud
existují) jsou popsány na <a href="releases/stable/">stránkách
o&nbsp;této verzi</a>.</p>

<p>Zvláště věnujte pozornost <a
href="releases/stable/releasenotes">poznámkám k&nbsp;vydání</a>
a&nbsp;<a href="releases/stable/errata">opravám (errata)</a>.</p>
