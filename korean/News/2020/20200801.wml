#use wml::debian::translation-check translation="846effa858f46cf6429f61e241ec96292032972f" mindelta="-1"
<define-tag pagetitle>데비안 10 업데이트: 10.5 릴리스</define-tag>
<define-tag release_date>2020-08-01</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.5</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>데비안 프로젝트는 안정 배포 데비안<release> (코드명 <q><codename></q>)의 다섯번째 업데이트를 알리게 되어 기쁩니다.
이 포인트 릴리스는 심각한 문제 조치 및 보안 이슈 수정을 주로 포함합니다.
보안 권고는 이미 개별적으로 알렸으며 가능한 곳에서 참조됩니다.</p>

<p>이 포인트 릴리스는 또한 데비안 보안 권고: 
<a href="https://www.debian.org/security/2020/dsa-4735">DSA-4735-1 grub2 -- 보안 업데이트</a> 
관련된 CVE 이슈 관련 <a href="https://www.debian.org/security/2020-GRUB-UEFI-SecureBoot/">GRUB2 UEFI SecureBoot 'BootHole' 취약점</a>도 해결합니다.</p>

<p>포인트 릴리스는 데비안 <release> 새 버전을 만드는 것이 아니며 포함된 일부 패키지만 업데이트함을 주의하세요.
옛 <q><codename></q> 미디어를 버릴 필요 없습니다.
설치 후, 패키지는 최신 데이안 미러에서 현재 버전으로 업그레이드 할 수 있습니다.</p>

<p>security.debian.org의 업데이트를 자주 설치하는 사람은 패키지를 많이 업데이트하지 않아도 되며, 해당 업데이트는 대부분 포인트 릴리스에 들어 있습니다.
</p>

<p>새 설치 위치는 정규 위치에 곧 공개될 겁니다.</p>

<p>패키지 관리 시스템이 수많은 데비안 HTTP 미러 중 하나를 가리키게 해서 기존 설치를 이 개정판으로 업그레이드할 수 있습니다.
포괄적인 미러 서버 목록은 아래에 있습니다:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>여러가지 버그 수정</h2>

<p>이 안정 업데이트는 다음 패키지에 몇 중요한 수정을 추가했습니다:</p>

<table border=0>
<tr><th>패키지</th>               <th>이유</th></tr>
<correction appstream-glib "Fix build failures in 2020 and later">
<correction asunder "Use gnudb instead of freedb by default">
<correction b43-fwcutter "Ensure removal succeeds under non-English locales; do not fail removal if some files no longer exist; fix missing dependencies on pciutils and ca-certificates">
<correction balsa "Provide server identity when validating certificates, allowing successful validation when using the glib-networking patch for CVE-2020-13645">
<correction base-files "Update for the point release">
<correction batik "Fix server-side request forgery via xlink:href attributes [CVE-2019-17566]">
<correction borgbackup "Fix index corruption bug leading to data loss">
<correction bundler "Update required version of ruby-molinillo">
<correction c-icap-modules "Add support for ClamAV 0.102">
<correction cacti "Fix issue where UNIX timestamps after September 13th 2020 were rejected as graph start / end; fix remote code execution [CVE-2020-7237], cross-site scripting [CVE-2020-7106], CSRF issue [CVE-2020-13231]; disabling a user account does not immediately invalidate permissions [CVE-2020-13230]">
<correction calamares-settings-debian "Enable displaymanager module, fixing autologin options; use xdg-user-dir to specify Desktop directory">
<correction clamav "New upstream release; security fixes [CVE-2020-3327 CVE-2020-3341 CVE-2020-3350 CVE-2020-3327 CVE-2020-3481]">
<correction cloud-init "New upstream release">
<correction commons-configuration2 "Prevent object creation when loading YAML files [CVE-2020-1953]">
<correction confget "Fix the Python module's handling of values containing <q>=</q>">
<correction dbus "New upstream stable release; prevent a denial of service issue [CVE-2020-12049]; prevent use-after-free if two usernames share a uid">
<correction debian-edu-config "Fix loss of dynamically allocated IPv4 address">
<correction debian-installer "Update Linux ABI to 4.19.0-10">
<correction debian-installer-netboot-images "Rebuild against proposed-updates">
<correction debian-ports-archive-keyring "Increase the expiration date of the 2020 key (84C573CD4E1AFD6C) by one year; add Debian Ports Archive Automatic Signing Key (2021); move the 2018 key (ID: 06AED62430CB581C) to the removed keyring">
<correction debian-security-support "Update support status of several packages">
<correction dpdk "New upstream release">
<correction exiv2 "Adjust overly restrictive security patch [CVE-2018-10958 and CVE-2018-10999]; fix denial of service issue [CVE-2018-16336]">
<correction fdroidserver "Fix Litecoin address validation">
<correction file-roller "Security fix [CVE-2020-11736]">
<correction freerdp2 "Fix smartcard logins; security fixes [CVE-2020-11521 CVE-2020-11522 CVE-2020-11523 CVE-2020-11524 CVE-2020-11525 CVE-2020-11526]">
<correction fwupd "New upstream release; fix possible signature verification issue [CVE-2020-10759]; use rotated Debian signing keys">
<correction fwupd-amd64-signed "New upstream release; fix possible signature verification issue [CVE-2020-10759]; use rotated Debian signing keys">
<correction fwupd-arm64-signed "New upstream release; fix possible signature verification issue [CVE-2020-10759]; use rotated Debian signing keys">
<correction fwupd-armhf-signed "New upstream release; fix possible signature verification issue [CVE-2020-10759]; use rotated Debian signing keys">
<correction fwupd-i386-signed "New upstream release; fix possible signature verification issue [CVE-2020-10759]; use rotated Debian signing keys">
<correction fwupdate "Use rotated Debian signing keys">
<correction fwupdate-amd64-signed "Use rotated Debian signing keys">
<correction fwupdate-arm64-signed "Use rotated Debian signing keys">
<correction fwupdate-armhf-signed "Use rotated Debian signing keys">
<correction fwupdate-i386-signed "Use rotated Debian signing keys">
<correction gist "Avoid deprecated authorization API">
<correction glib-networking "Return bad identity error if identity is unset [CVE-2020-13645]; break balsa older than 2.5.6-2+deb10u1 as the fix for CVE-2020-13645 breaks balsa's certificate verification">
<correction gnutls28 "Fix TL1.2 resumption errors; fix memory leak; handle zero length session tickets, fixing connection errors on TLS1.2 sessions to some big hosting providers; fix verification error with alternate chains">
<correction intel-microcode "Downgrade some microcodes to previously issued versions, working around hangs on boot on Skylake-U/Y and Skylake Xeon E3">
<correction jackson-databind "Fix multiple security issues affecting BeanDeserializerFactory [CVE-2020-9548 CVE-2020-9547 CVE-2020-9546 CVE-2020-8840 CVE-2020-14195 CVE-2020-14062 CVE-2020-14061 CVE-2020-14060 CVE-2020-11620 CVE-2020-11619 CVE-2020-11113 CVE-2020-11112 CVE-2020-11111 CVE-2020-10969 CVE-2020-10968 CVE-2020-10673 CVE-2020-10672 CVE-2019-20330 CVE-2019-17531 and CVE-2019-17267]">
<correction jameica "Add mckoisqldb to classpath, allowing use of SynTAX plugin">
<correction jigdo "Fix HTTPS support in jigdo-lite and jigdo-mirror">
<correction ksh "Fix environment variable restriction issue [CVE-2019-14868]">
<correction lemonldap-ng "Fix nginx configuration regression introduced by the fix for CVE-2019-19791">
<correction libapache-mod-jk "Rename Apache configuration file so it can be automatically enabled and disabled">
<correction libclamunrar "New upstream stable release; add an unversioned meta-package">
<correction libembperl-perl "Handle error pages from Apache &gt;= 2.4.40">
<correction libexif "Security fixes [CVE-2020-12767 CVE-2020-0093 CVE-2020-13112 CVE-2020-13113 CVE-2020-13114]; fix buffer overflow [CVE-2020-0182] and integer overflow [CVE-2020-0198]">
<correction libinput "Quirks: add trackpoint integration attribute">
<correction libntlm "Fix buffer overflow [CVE-2019-17455]">
<correction libpam-radius-auth "Fix buffer overflow in password field [CVE-2015-9542]">
<correction libunwind "Fix segfaults on mips; manually enable C++ exception support only on i386 and amd64">
<correction libyang "Fix cache corruption crash, CVE-2019-19333, CVE-2019-19334">
<correction linux "New upstream stable release">
<correction linux-latest "Update for 4.19.0-10 kernel ABI">
<correction linux-signed-amd64 "New upstream stable release">
<correction linux-signed-arm64 "New upstream stable release">
<correction linux-signed-i386 "New upstream stable release">
<correction lirc "Fix conffile management">
<correction mailutils "maidag: drop setuid privileges for all delivery operations but mda [CVE-2019-18862]">
<correction mariadb-10.3 "New upstream stable release; security fixes [CVE-2020-2752 CVE-2020-2760 CVE-2020-2812 CVE-2020-2814 CVE-2020-13249]; fix regression in RocksDB ZSTD detection">
<correction mod-gnutls "Fix a possible segfault on failed TLS handshake; fix test failures">
<correction multipath-tools "kpartx: use correct path to partx in udev rule">
<correction mutt "Don't check IMAP PREAUTH encryption if $tunnel is in use">
<correction mydumper "Link against libm">
<correction nfs-utils "statd: take user-id from /var/lib/nfs/sm [CVE-2019-3689]; don't make /var/lib/nfs owned by statd">
<correction nginx "Fix error page request smuggling vulnerability [CVE-2019-20372]">
<correction nmap "Update default key size to 2048 bits">
<correction node-dot-prop "Fix regression introduced in CVE-2020-8116 fix">
<correction node-handlebars "Disallow calling <q>helperMissing</q> and <q>blockHelperMissing</q> directly [CVE-2019-19919]">
<correction node-minimist "Fix prototype pollution [CVE-2020-7598]">
<correction nvidia-graphics-drivers "New upstream stable release; security fixes [CVE-2020-5963 CVE-2020-5967]">
<correction nvidia-graphics-drivers-legacy-390xx "New upstream stable release; security fixes [CVE-2020-5963 CVE-2020-5967]">
<correction openstack-debian-images "Install resolvconf if installing cloud-init">
<correction pagekite "Avoid issues with expiry of shipped SSL certificates by using those from the ca-certificates package">
<correction pdfchain "Fix crash at startup">
<correction perl "Fix multiple regular expression related security issues [CVE-2020-10543 CVE-2020-10878 CVE-2020-12723]">
<correction php-horde "Fix cross-site scripting vulnerability [CVE-2020-8035]">
<correction php-horde-gollem "Fix cross-site scripting vulnerability in breadcrumb output [CVE-2020-8034]">
<correction pillow "Fix multiple out-of-bounds read issues [CVE-2020-11538 CVE-2020-10378 CVE-2020-10177]">
<correction policyd-rate-limit "Fix issues in accounting due to socket reuse">
<correction postfix "New upstream stable release; fix segfault in the tlsproxy client role when the server role was disabled; fix <q>maillog_file_rotate_suffix default value used the minute instead of the month</q>; fix several TLS related issues; README.Debian fixes">
<correction python-markdown2 "Fix cross-site scripting issue [CVE-2020-11888]">
<correction python3.7 "Avoid infinite loop when reading specially crafted TAR files using the tarfile module [CVE-2019-20907]; resolve hash collisions for IPv4Interface and IPv6Interface [CVE-2020-14422]; fix denial of service issue in urllib.request.AbstractBasicAuthHandler [CVE-2020-8492]">
<correction qdirstat "Fix saving of user-configured MIME categories">
<correction raspi3-firmware "Fix typo that could lead to unbootable systems">
<correction resource-agents "IPsrcaddr: make <q>proto</q> optional to fix regression when used without NetworkManager">
<correction ruby-json "Fix unsafe object creation vulnerability [CVE-2020-10663]">
<correction shim "Use rotated Debian signing keys">
<correction shim-helpers-amd64-signed "Use rotated Debian signing keys">
<correction shim-helpers-arm64-signed "Use rotated Debian signing keys">
<correction shim-helpers-i386-signed "Use rotated Debian signing keys">
<correction speedtest-cli "Pass correct headers to fix upload speed test">
<correction ssvnc "Fix out-of-bounds write [CVE-2018-20020], infinite loop [CVE-2018-20021], improper initialisation [CVE-2018-20022], potential denial-of-service [CVE-2018-20024]">
<correction storebackup "Fix possible privilege escalation vulnerability [CVE-2020-7040]">
<correction suricata "Fix dropping privileges in nflog runmode">
<correction tigervnc "Don't use libunwind on armel, armhf or arm64">
<correction transmission "Fix possible denial of service issue [CVE-2018-10756]">
<correction wav2cdr "Use C99 fixed-size integer types to fix runtime assertion on 64bit architectures other than amd64 and alpha">
<correction zipios++ "Security fix [CVE-2019-13453]">
</table>


<h2>보안 업데이트</h2>


<p>이 리비전은 다음 보안 업데이트를 안정 릴리스에 추가합니다.
보안 팀음 이미 각 업데이트에 대해 권고를 공개했습니다.:</p>

<table border=0>
<tr><th>권고 ID</th>  <th>패키지</th></tr>
<dsa 2020 4626 php7.3>
<dsa 2020 4674 roundcube>
<dsa 2020 4675 graphicsmagick>
<dsa 2020 4676 salt>
<dsa 2020 4677 wordpress>
<dsa 2020 4678 firefox-esr>
<dsa 2020 4679 keystone>
<dsa 2020 4680 tomcat9>
<dsa 2020 4681 webkit2gtk>
<dsa 2020 4682 squid>
<dsa 2020 4683 thunderbird>
<dsa 2020 4684 libreswan>
<dsa 2020 4685 apt>
<dsa 2020 4686 apache-log4j1.2>
<dsa 2020 4687 exim4>
<dsa 2020 4688 dpdk>
<dsa 2020 4689 bind9>
<dsa 2020 4690 dovecot>
<dsa 2020 4691 pdns-recursor>
<dsa 2020 4692 netqmail>
<dsa 2020 4694 unbound>
<dsa 2020 4695 firefox-esr>
<dsa 2020 4696 nodejs>
<dsa 2020 4697 gnutls28>
<dsa 2020 4699 linux-signed-amd64>
<dsa 2020 4699 linux-signed-arm64>
<dsa 2020 4699 linux-signed-i386>
<dsa 2020 4699 linux>
<dsa 2020 4700 roundcube>
<dsa 2020 4701 intel-microcode>
<dsa 2020 4702 thunderbird>
<dsa 2020 4704 vlc>
<dsa 2020 4705 python-django>
<dsa 2020 4707 mutt>
<dsa 2020 4708 neomutt>
<dsa 2020 4709 wordpress>
<dsa 2020 4710 trafficserver>
<dsa 2020 4711 coturn>
<dsa 2020 4712 imagemagick>
<dsa 2020 4713 firefox-esr>
<dsa 2020 4714 chromium>
<dsa 2020 4716 docker.io>
<dsa 2020 4718 thunderbird>
<dsa 2020 4719 php7.3>
<dsa 2020 4720 roundcube>
<dsa 2020 4721 ruby2.5>
<dsa 2020 4722 ffmpeg>
<dsa 2020 4723 xen>
<dsa 2020 4724 webkit2gtk>
<dsa 2020 4725 evolution-data-server>
<dsa 2020 4726 nss>
<dsa 2020 4727 tomcat9>
<dsa 2020 4728 qemu>
<dsa 2020 4729 libopenmpt>
<dsa 2020 4730 ruby-sanitize>
<dsa 2020 4731 redis>
<dsa 2020 4732 squid>
<dsa 2020 4733 qemu>
<dsa 2020 4735 grub-efi-amd64-signed>
<dsa 2020 4735 grub-efi-arm64-signed>
<dsa 2020 4735 grub-efi-ia32-signed>
<dsa 2020 4735 grub2>
</table>


<h2>삭제된 패키지</h2>

<p>다음 패키지는 우리 제어를 넘어 삭제되었습니다:</p>

<table border=0>
<tr><th>패키지</th>               <th>이유</th></tr>
<correction golang-github-unknwon-cae "Security issues; unmaintained">
<correction janus "Not supportable in stable">
<correction mathematica-fonts "Relies on unavailable download location">
<correction matrix-synapse "Security issues; unsupportable">
<correction selenium-firefoxdriver "Incompatible with newer Firefox ESR versions">

</table>

<h2>데비안 설치관리자</h2>
<p>설치 관리자는 포인트 릴리스에서 안정 릴리스와 합쳐진 수정 사항을 포함하도록 업데이트 되었습니다.
</p>

<h2>URL</h2>

<p>이 리비전에서 바뀐 패키지 목록:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>현재 안정 배포판:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Proposed updates to the stable distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>stable distribution information (release notes, errata etc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Security announcements and information:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>데비안은</h2>

<p>데비안 프로젝트는 완전한 자유 운영체제인 데비안을 제작하기 위해 자신의 시간과 노력을 자원하는 자유 소프트웨어 개발자의 모임입니다.
</p>

<h2>연락 정보</h2>

<p>보다 많은 정보를 원하면 <a href="$(HOME)/">https://www.debian.org/</a>에 있는 데비안 웹 페이지를 방문하거나, 
&lt;press@debian.org&gt;로 이메일을 보내세요. 또 &lt;debian-release@lists.debian.org&gt;로 보내서 안정 릴리스 팀으로 연락하세요.
</p>
