<define-tag pagetitle>데비안 10 <q>buster</q> 릴리스</define-tag>
<define-tag release_date>2019-07-06</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="cbfcd50b343ab08499a29ae770a5528eda687986" mindelta="-1"
# 주의: 불완전한 번역. 번역을 마친 다음 위의 'mindelta="-1"'을 지우십시오.

<p>25달 동안 개발 후에 데비안 프로젝트는 새 안정 버전 10(코드명 <q>buster</q>)을 선물하는 것을 자랑스럽게 생각하며,
5년간 지원될 <a href="https://security-team.debian.org/">데비안 보안 팀</a>과 <a href="https://wiki.debian.org/LTS">데비안 장기 지원</a>팀
의 통합 작업 고맙습니다.</p>

<p>데비안 10 <q>buster</q>는 여러 데스크톱 응용프로그램과 환경과 함께 나옵니다. 아래 데스크톱 환경도 포함합니다:
</p>
<ul>
<li>Cinnamon 3.8,</li>
<li>GNOME 3.30,</li>
<li>KDE Plasma 5.14,</li>
<li>LXDE 0.99.2,</li>
<li>LXQt 0.14,</li>
<li>MATE 1.20,</li>
<li>Xfce 4.12.</li>
</ul>

<p>이 릴리스에서는 GNOME은 Wayland 디스플레이 서버를 Xorg 대신 기본으로 사용합니다. 
Wayland는 더 단순하고 현대적인 디자인을 가지고 있습니다. 이것은 보안에 이점이 있습니다. 
그러나 Xorg 디스플레이 서버는 여전히 기본으로 설치되어 있으며,
기본 디스플레이 관리자에서 사용자가 다음 세션의 디스플레이 서버로 Xorg를 선택할 수 있습니다.
</p>

<p>재현 가능한 빌드 프로젝트 덕분에, 데비안 10에 포함된 소스 패키지의 91% 이상이 비트마다 동일한 바이너리 패키지를 구축할 겁니다.
이것은 컴파일러를 조작하고 네트워크를 구축하려는 악의적인 시도로부터 사용자를 보호하는 중요한 검증 기능입니다.
미래 데비안 릴리스는 최종 사용자가 아카이브 내에서 패키지의 입증 가능성을 검증할 수 있도록 툴과 메타데이터를 포함할 겁니다.
</p>

<p>
보안에 민감한 환경에서, 프로그램 기능을 제한하기 위한 필수 액세스 제어 프레임워크인 앱 AppArmor이 기본적으로 설치되고 활성화됩니다.
게다가, APT에서 제공하는 모든 방법(cdrom, gpgv 및 rsh 제외)은 선택적으로 <q>seccomp-BPF</q> 샌드박스를 사용할 수 있습니다.
APT용 https 방법은 적절한 패키지에 포함되어 있으므로 따로 설치할 필요 없습니다.
</p>

<p>
네트워크 필터링은 기본적으로 데비안 10 <q>buster</q>에서 nftables 프레임워크에 기초합니다.
iptables v1.8.2부터 시작하는 이진 패키지는 iptables-nft 및 iptables-legacy 두 가지 iptables 명령줄 인터페이스 버전을 포함합니다. 
nftables 기반 변형은 nf_tables Linux 커널 하위 시스템을 사용합니다. 
<q>대체</q> 시스템을 써서 변형 중에 선택할 수 있습니다.
</p>

<p>
데비안 7(코드명 <q>wheezy</q>)에서 처음 도입된 UEFI(Unified Extensible Firmware Interface) 지원은 
데비안 10 <q>buster</q>에서도 계속 크게 개선되고 있습니다. 
보안 부팅 지원이 i386과 arm64amd64 아키텍처를 위해 이 릴리스에 포함되었으며 
대부분의 안전 부트 가능 기계에서 동작해야 할 겁니다.
이는 사용자가 더 이상 펌웨어 구성에서 Secure Boot 지원을 비활성화할 필요가 없음을 뜻합니다.
</p>

<p>
cups와 cups-filters 패키지는 기본적으로 데비안 10 <q>buster</q>에 설치되며, 
사용자에게 드라이버 없는 인쇄를 활용하는 데 필요한 모든 것을 제공합니다. 
네트워크 인쇄 큐와 IPP 프린터는 cups-browsed로 자동으로 설정 및 관리되며, 
비자유 공급업체 인쇄 드라이버와 플러그인을 사용할 수 있습니다.
</p>

<p>데비안 10 <q>buster</q>에는 업데이트된 수많은 소프트웨어 패키지(이전 릴리스의 모든 패키지의 62% 이상)가 포함되어 있으며, 다음과 같습니다:
</p>
<ul>
<li>Apache 2.4.38</li>
<li>BIND DNS Server 9.11</li>
<li>Chromium 73.0</li>
<li>Emacs 26.1</li>
<li>Firefox 60.7 (in the firefox-esr package)</li>
<li>GIMP 2.10.8</li>
<li>GNU Compiler Collection 7.4 and 8.3</li>
<li>GnuPG 2.2</li>
<li>Golang 1.11</li>
<li>Inkscape 0.92.4</li>
<li>LibreOffice 6.1</li>
<li>Linux 4.19 series</li>
<li>MariaDB 10.3</li>
<li>OpenJDK 11</li>
<li>Perl 5.28</li>
<li>PHP 7.3</li>
<li>PostgreSQL 11</li>
<li>Python 3 3.7.2</li>
<li>Ruby 2.5.1</li>
<li>Rustc 1.34</li>
<li>Samba 4.9</li>
<li>systemd 241</li>
<li>Thunderbird 60.7.2</li>
<li>Vim 8.1</li>
<li>약 29,000개 소스 패키지에서 만들어진 59,000개 넘는 쓸 준비된 패키지.</li>
</ul>

<p>
이러한 광범위한 패키지 선택과 그것의 전통적인 넓은 아키텍처 지원으로, 데비안은 다시 한번 보편적인 운영체제가 되겠다는 그 목표에 충실합니다.
데스크톱 시스템에서 넷북, 개발 서버에서 클러스터 시스템, 데이터베이스, 웹 및 스토리지 서버 등 다양한 사용 사례에 적합합니다.
데비안 아카이브의 모든 패키지에 대한 자동 설치 및 업그레이드 테스트와 같은 추가적인 품질 보증 노력은 
<q>buster</q>가 사용자가 안정적인 데비안 릴리스에 대해 가지고 있는 높은 기대를 충족하도록 보장합니다.
</p>

<p>전체 10개 아키텍처 지원:
64-bit PC / Intel EM64T / x86-64 (<code>amd64</code>),
32-bit PC / Intel IA-32 (<code>i386</code>),
64-bit little-endian Motorola/IBM PowerPC (<code>ppc64el</code>),
64-bit IBM S/390 (<code>s390x</code>),
ARM 용, <code>armel</code>
및 <code>armhf</code> 오래된 그리고 좀 더 최근 32-bit 하드웨어,
추가로 <code>arm64</code> 64-비트 <q>AArch64</q> 아키텍처 용,
그리고 MIPS 용, <code>mips</code> (big-endian) 
및 <code>mipsel</code> (little-endian) 아키텍처 32-비트 하드웨어용 
및 <code>mips64el</code> 64-bit little-endian 하드웨어 용.
</p>

<h3>써보고 싶나요?</h3>
<p>
데비안 10 <q>buster</q>를 설치하지 않고 써보고 싶다면,
사용 가능한 <a href="$(HOME)/CD/live/">라이브 이미지</a> 중 하나를 써서 전체 운영 체제를 컴퓨터 메모리에 읽기 전용 상태로 로드하고 실행할 수 있습니다.
</p>

<p>
이러한 라이브 이미지는 <code>amd64</code> 및 <code>i386</code> 아키텍처에 제공되며 DVD, USB 스틱 및 넷부팅 설정에 사용할 수 있습니다.
사용자는 다양한 데스크톱 환경 중에서 시나몬, GNOME, KDE Plasma, LXDE, 마테, Xfce 및 새로운 buster, LXQt를 선택할 수 있습니다.
Debian Live Buster는 표준 라이브 이미지를 다시 도입하므로 그래픽 사용자 인터페이스 없이 기본 데비안 시스템을 써볼 수도 있습니다.
</p>
<p>
운영 체제를 즐기는 경우 라이브 이미지에서 컴퓨터의 하드 디스크에 설치할 수 있습니다.
라이브 이미지는 Calamares 독립 설치 관리자 및 표준 데비안 설치 관리자가 포함합니다.
자세한 내용은 데비안 웹 사이트에서 <a href="$(HOME)/releases/buster/releasenotes">릴리스 노트</a> 및
<a href="$(HOME)/CD/live/">리이브 설치 이미지</a> 섹션에서 확인할 수 있습니다.
</p>

<p>
데비안 10 <q>buster</q>를 컴퓨터에 직접 설치하려면
다양한 설치 미디어 예를 들어, 블루레이 디스크, DVD, CD, USB 스틱 또는 네트워크 연결 등 중에 선택할 수 있습니다.
여러 데스크톱 환경 &mdash; Cinnamon, GNOME, KDE Plasma Desktop 및 
Applications, LXDE, LXQt, MATE 및 Xfce &mdash; 이들 이미지를 통해 설치될 수 있을 겁니다.
게다가,  
단일 디스크에서 선택한 아키텍처의 설치를 지원하는 <q>다중 아키텍처</q>를 사용할 수 있습니다. 
또는 부팅 가능한 USB 설치 미디어를 언제나 만들 수 있습니다 (자세한 내용은 <a href="$(HOME)/releases/buster/installmanual">설치 가이드</a> 참조).
</p>

<p>클라우드 사용자를 위해 데비안은 가장 잘 알려진 많은 클라우드 플랫폼을 직접 지원합니다.
공식 데비안 이미지는 각 이미지 시장을 통해 쉽게 선택됩니다. Debian also publishes <a
href="https://cloud.debian.org/images/openstack/current/">pre-built
OpenStack images</a> for the <code>amd64</code> and <code>arm64</code>
architectures, ready to download and use in local cloud setups.
</p>

<p>
이제 76개 언어로 데비안을 설치할 수 있으며, 대부분 텍스트 기반 및 그래픽 사용자 인터페이스에서 사용할 수 있습니다.
</p>

<p>
설치 이미지는 <a href="$(HOME)/CD/torrent-cd/">bittorrent</a> (권장 방법),
<a href="$(HOME)/CD/jigdo-cd/#which">jigdo</a>, 또는
<a href="$(HOME)/CD/http-ftp/">HTTP</a>를 통해 지금 바로 받을 수 있습니다.
자세한 내용은 <a href="$(HOME)/CD/">데비안 CD</a>를 참조하십시오. 버스터는 곧 물리적 DVD, CD-ROM, 블루레이 디스크에서도 구입할 수 있을 겁니다.

<a href="$(HOME)/CD/">Debian on CDs</a> for further information. <q>Buster</q> will
soon be available on physical DVD, CD-ROM, and Blu-ray Discs from
numerous <a href="$(HOME)/CD/vendors">vendors</a> too.
</p>


<h3>데비안 업그레이드</h3>
<p>
이전 릴리스 데비안 9(코드명 <q>stretch</q>)에서 데비안 10으로 업그레이드는 apt 패키지 관리 툴에 의해 자동 처리됩니다.
항상 그렇듯이, 데비안 시스템은 어떠한 강제적인 다운타임 없이 고통 없이, 제 자리에서 업그레이드될 수 있지만, 
가능한 문제에 대해서는 <a href="$(HOME)/releases/buster/releasenotes">release notes</a>와 
<a href="$(HOME)/releases/buster/installmanual">installation guide</a>를 읽는 것을 강력히 권장합니다.
릴리스 노트는 더욱 개선되어 릴리스 후 몇 주 내에 추가 언어로 번역될 겁니다.
</p>


<h2>데비안에 대해</h2>

<p>
데비안은 인터넷을 통해 공동작업을 하는 전 세계 수천 명의 자원봉사자들에 의해 개발된 자유 운영 체제입니다.
데비안 프로젝트의 주요 강점은 자원 봉사 기반, Debian Social Contract 및 Free Software에 대한 헌신, 그리고 가능한 최고의 운영 체제를 제공하겠다는 약속입니다.
이 새로운 릴리스는 그 방향에서 또 하나의 중요한 단계입니다.
</p>


<h2>연락 정보</h2>

<p>더 많은 정보는 데비안 웹 페이지 <a href="$(HOME)/">https://www.debian.org/</a>를 방문하거나 
&lt;press@debian.org&gt;에 메일을 보내세요
</p>
