#use wml::debian::template title="데비안 11 -- 정오표" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="98c1c344bcb8cddb17f6d0dd7b4de8a361aa297f" maintainer="Changwoo Ryu"

#include "$(ENGLISHDIR)/releases/info"

<toc-display/>


# <toc-add-entry name="known_probs">Known problems</toc-add-entry>
<toc-add-entry name="security">보안 이슈</toc-add-entry>

<p>데비안 보안 팀은 안정 릴리스 패키지에 대한 업데이트를 발행합니다. 이 업데이트에서는 식별한 보안 관련 문제에
대응합니다. <q>bullseye</q>에서 대응한 보안 이슈는
<a href="$(HOME)/security/">보안 페이지</a>를 보세요.</p>

<p>APT를 쓴다면, 최근 보안 업데이트에 접근하려면 아래 행을 <tt>/etc/apt/sources.list</tt>에 추가하십시오:</p>

<pre>
  deb http://security.debian.org/debian-security bullseye-security main contrib non-free
</pre>

<p>그 다음, <kbd>apt update</kbd> 명령과
<kbd>apt upgrade</kbd> 명령을 차례로 실행하십시오.</p>


<toc-add-entry name="pointrelease">포인트 릴리스</toc-add-entry>

<p>때때로, 심각한 문제 또는 보안 업데이트가 여럿 있으면
릴리스 배포가 업데이트 됩니다. 일반적으로, 포인트 릴리스로 표시됩니다.</p>

<ul>
  <li>첫 포인트 릴리스, 11.1은
      <a href="$(HOME)/News/2021/20211009">2021년 10월 9일</a>에 릴리스되었습니다.</li>
  <li>두번째 포인트 릴리스, 11.2는
      <a href="$(HOME)/News/2021/20211218">2021년 12월 18일</a>에 릴리스되었습니다.</li>
  <li>세번째 포인트 릴리스, 11.3은
      <a href="$(HOME)/News/2022/20220326">2022년 3월 26일</a>에 릴리스되었습니다.</li>
  <li>네번째 포인트 릴리스, 11.4는
      <a href="$(HOME)/News/2022/20220326">2022년 7월 9일</a>에 릴리스되었습니다.</li>
</ul>

<ifeq <current_release_bullseye> 11.0 "

<p>아직 데비안 11의 포인트 릴리스가 없습니다.</p>" "

<p>데비안 11과 <current_release_bullseye/> 차이를 자세히 보려면 <a
href="http://http.us.debian.org/debian/dists/bullseye/ChangeLog">\
ChangeLog</a> 를 보세요.</p>"/>


<p>릴리스된 안정 버전 배포판의 수정 사항은 아카이브에 반영되기 전에 확장된 시험 기간을
거칩니다. 하지만 이 수정 사항은 모든 데비안 아카이브 미러 사이트의
<a href="http://ftp.debian.org/debian/dists/bullseye-proposed-updates/">\
dists/bullseye-proposed-updates</a> 디렉터리에 들어 있습니다.</p>

<p>apt를 써서 패키지를 업데이트하면, 다음 줄을
<tt>/etc/apt/sources.list</tt>에 추가하여 제안된 업데이트를 설치할 수 있습니다:</p>

<pre>
  \# proposed additions for a 11 point release
  deb http://deb.debian.org/debian bullseye-proposed-updates main contrib non-free
</pre>

<p>그 다음, <kbd>apt update</kbd> 명령과
<kbd>apt upgrade</kbd> 명령을 차례로 실행하십시오.</p>


<toc-add-entry name="installer">설치 시스템</toc-add-entry>

<p>
설치 시스템의 정오표 및 업데이트 정보는 <a href="debian-installer/">설치 정보</a> 페이지를 보십시오.
</p>
