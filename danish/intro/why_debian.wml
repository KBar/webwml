#use wml::debian::template title="Grunde til at vælge Debian" MAINPAGE="true"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="5eef343480ec39f5bb8db5e4e69bbb9e8b926c9b"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#users">Debian for brugere</a></li>
    <li><a href="#devel">Debian for udviklere</a></li>
    <li><a href="#enterprise">Debian for virksomheder</a></li>
  </ul>
</div>

<p>Der er mange årsager til at vælge Debian som ens styresystem – som en bruger, 
som en udvikler og endda som en virksomhed.  De fleste brugere sætter pris på 
stabiliteten og den enkle opgraderingsproces af både pakker og hele 
distributionen.  Debian er også meget udbredt blandt software- og 
hardwareudviklere, på grund af at den kører på talrige arkitekturer og enheder, 
har en offentligt tilgængelig fejlsporing og andre værktøjer til udviklere.  
Hvis du planlægger at anvende Debian i et professionelt miljø, så er der 
yderligere fordel så som LTS-versioner og cloudfilaftryk.</p>


<aside>
<p><span class="far fa-comment-dots fa-3x"></span> For mig er der det pefekte niveau af letanvendelighed og stabilitet. Jeg har anvendt flere forskellige distributioner i årenes løb, men Debian er den eneste, som bare fungerer. <a href="https://www.reddit.com/r/debian/comments/8r6d0o/why_debian/e0osmxx">NorhamsFinest på Reddit</a></p>
</aside>

<h2><a id="users">Debian for brugere</a></h2>

<dl>
  <dt><strong>Debian er fri software.</strong></dt>
  <dd>
    Debian består af fri og open source-software, og vil altid være 100 procent
    <a href="free">fri</a>.  Fri for alle til at anvende, ændre og distribuere. 
    Det er vores primære løfte til <a href="../users">vores brugere</a>.  Det er 
    også omkostningsfrit.
  </dd>
</dl>

<dl>
  <dt><strong>Debian er stabil og sikker.</strong></dt>
  <dd>
    Debian er et Linux-baseret styresystem for et bredt udvalg af enheder, 
    herunder bærbare computere, stationære computere og servere.  Vi leverer 
    fornuftige standardindstillinger for enhver pakke samt jævnlige 
    sikkerhedsopdateringer i pakkernes levetid.
  </dd>
</dl>

<dl>
  <dt><strong>Debian har omfattende understøttelse af hardware.</strong></dt>
  <dd>
    Det meste hardware er allerede understøttet af Linux-kernen, hvilket betyder 
    at Debian også understøtter den.  Proprietære drivere til hardware er om 
    nødvendigt tilgængelige.
  </dd>
</dl>

<dl>
  <dt><strong>Debian har et fleksibelt installeringsprogram.</strong></dt>
  <dd>
    Vores 
    <a href="https://cdimage.debian.org/images/release/current-live/amd64/iso-hybrid/">\
    Live-CD</a> er beregnet til alle, der også at afprøve Debian før de 
    installerer det.  Der medfølger også Calamares-installeringsprogrammet, som 
    gør det let at installere Debian fra et livesystem.  Mere erfarne brugere 
    kan anvendes Debians installeringsprogram, med flere valgmuligheder til 
    finindstilling, herunder mulighed for automatiseret netværksinstallering.
  </dd>
</dl>

<dl>
  <dt><strong>Debian giver problemfrie opgraderinger.</strong></dt>
  <dd>
    Det er let at holde vores styresystem opdateret, uanset om du ønsker at 
    opgradere til en helt ny udgave eller blot at opdatere en enkelt pakke.
  </dd>
</dl>

<dl>
  <dt><strong>Debian er grundlag for mange andre distributioner.</strong></dt>
  <dd>
    Mange populære Linux-distributioner, så som Ubuntu, Knoppix, PureOS og 
    Tails, er baseret på Debian.  Vi stiller alle værktøjer til rådighed, så 
    alle kan supplere softwarepakkerne fra Debians arkiv med deres egne pakker, 
    som opfylder deres behov, hvis der er behov for det.
  </dd>
</dl>

<dl>
  <dt><strong>Debian-projektet er et fællesskab.</strong></dt>
  <dd>
    Alle kan blive en del af vores fællesskab; du behøver ikke at være udvikler 
    eller systemadministrator.  Debian har en <a href="../devel/constitution">\
    demokratisk styreform</a>.  Da alle medlemmer af Debian-projektet har de 
    samme rettigheder, kan Debian ikke kontrolleres af en enkelt virksomhed. 
    Vores udviklere kommer fra flere end 60 forskellige lande, og Debian selv 
    er oversat til flere end 80 sprog.
  </dd>
</dl>


<aside>
<p><span class="far fa-comment-dots fa-3x"></span> Årsagen til Debians status som et udviklerstyresystem, er det store antal pakker og softwareunderstøttelse, som er vigtigt for udviklere.  Det anbefales kraftigt til avancerede programmører og systemadministratorer. <a href="https://fossbytes.com/best-linux-distros-for-programming-developers/">Adarsh Verma på Fossbytes</a></p>
</aside>

<h2><a id="devel">Debian for udviklere</a></h2>

<dl>
  <dt><strong>Mange hardwarearkitekturer.</strong></dt>
  <dd>
    Debian understøtter en <a href="../ports">lang liste</a> af
    CPU-arkitekturer, herunder amd64, i386, mange versioner af ARM og MIPS, 
    POWER7, POWER8, IBM System z og RISC-V.  Debian er også tilgængelig for 
    ældre og specifikke nichearkitekturer.
  </dd>
</dl>

<dl>
  <dt><strong>IoT og indlejrede enheder.</strong></dt>
  <dd>
    Debian kører på et bredt udvalg af enheder, som eksempelvis Raspberry Pi, 
    forskellige varianter af QNAP, mobile enheder, hjemmerouter og mange 
    Single Board Computers (SBC).
  </dd>
</dl>

<dl>
  <dt><strong>Enormt antal softwarepakker.</strong></dt>
  <dd>
    Debian har et stort antal <a href="$(DISTRIB)/packages">pakker</a> 
    (i øjeblikket i den stabile distribution: <packages_in_stable> pakker), som 
    anvender <a href="https://manpages.debian.org/unstable/dpkg-dev/deb.5.en.html">\
    deb-formatet</a>.
  </dd>
</dl>

<dl>
  <dt><strong>Forskellige udgivelser.</strong></dt>
  <dd>
    Ud over vores stabile udgave, kan du installere nyere softwareversioner ved 
    at anvende udgaverne testing eller unstable.
  </dd>
</dl>

<dl>
  <dt><strong>Offentlig fejlsporing.</strong></dt>
  <dd>
    Vores Debian-<a href="../Bugs">fejlsporingssystem</a> (BTS) er offentligt 
    tilgængeligt for alle gennem en webbrowser.  Vi skjuler ikke vores 
    softwarefejl, og du kan let indsende nye fejlrapporter eller deltage i 
    debatten.
  </dd>
</dl>

<dl>
  <dt><strong>Fremgangsmåder og udviklerværktøjer.</strong></dt>
  <dd>
    Debian tilbyder software af høj kvalitet.  For at få mere at vide om vores 
    standarder, kan du læse vores <a href="../doc/debian-policy/">\
    fremgangsmåder</a>, som definerer de tekniske krav, som alle pakker skal 
    opfylde, for at kunne blive medtaget i distributionen.  Vores continuous 
    integration kører softwaren Autopkgtest (kører test mod pakker), Piuparts 
    (tester installering, opgradering og fjernelse) og Lintian (tjekker pakker 
    for uoverensstemmelser og fejl).
  </dd>
</dl>


<aside>
<p><span class="far fa-comment-dots fa-3x"></span> Stabilitet er synonymt med Debian. [...]  Sikkerhed er en af de vigtigste ting, som Debian tilbyder. <a href="https://www.pontikis.net/blog/five-reasons-to-use-debian-as-a-server">Christos Pontikis på pontikis.net</a></p>
</aside>

<h2><a id="enterprise">Debian til virksomheder</a></h2>

<dl>
  <dt><strong>Debian er pålidelig.</strong></dt>
  <dd>
    Debian beviser dagligt sin pålidelighed i tusindvis af scenarier i den 
    virkelige verden, spændende fra enkeltbrugeres bærbare computere til 
    super-collidere, børser og bilindustrien.  Det er også populært i den 
    akademiske verden, i videnskabelige miljøer og i den offentlige sektor.
  </dd>
</dl>

<dl>
  <dt><strong>Debian har mange eksperter.</strong></dt>
  <dd>
    Vores pakkevedligeholdere tager sig ikke kun af pakning i Debian og 
    indarbejdelse af nye opstrømsversioner.  Ofte er de eksperter i 
    applikationen selv, og bidrager derfor direkte til opstrømsudviklingen.  
  </dd>
</dl>

<dl>
  <dt><strong>Debian er sikker.</strong></dt>
  <dd>
    Debian har sikkerhedsunderstøttelse af sine stabile udgivelser.  Mange andre 
    distributioner og sikkerhedsefterforskere, er afhængige af Debians 
    sikkerhedssporingsværktøj.
  </dd>
</dl>

<dl>
  <dt><strong>Long Term Support.</strong></dt>
  <dd>
    Debians gratis <a href="https://wiki.debian.org/LTS">Long Term 
    Support</a>-version (LTS, langtidsunderstøttelse) forlænger levetiden på 
    alle Debians stabile udgivelser til minimum fem år.  Desuden er der det 
    kommercielle <a href="https://wiki.debian.org/LTS/Extended">Extended 
    LTS</a>-initiativ, som forlænger understøttelse af et begrænset antal pakker 
    i mere end fem år.
  </dd>
</dl>

<dl>
  <dt><strong>Cloudfilaftryk.</strong></dt>
  <dd>
    Officielle cloudfilaftryk er tilgængelige hos alle de store cloudplatforme. 
    Vi tilbyder også værktøjer og opsætninger, så du kan opbygge dit eget 
    skræddersyede cloudfilaftryk.  Du kan også anvende Debian i virtuelle 
    maskiner på desktop'en eller i en container.
  </dd>
</dl>
