#use wml::debian::template title="Espelhamento push"
#use wml::debian::translation-check translation="82968214595db755ef0c09f5d6c1b11a6660b5cd"

<p>Espelhamento push (push mirroring) é uma forma de espelhamento que minimiza
o tempo que as alterações no repositório levam para alcançarem os espelhos. O
servidor master usa um mecanismo de ativação para imediatamente informar o
espelho cliente que ele precisa ser atualizado.</p>

<p>O espelhamento push demanda maior esforço para ser configurado já que os(as)
mantenedores(as) do espelho de origem e de destino devem trocar informações. O
benefício está em que o espelho de origem inicia o processo de espelhamento
imediatamente após seu repositório ter sido atualizado. Isto permite que as
alterações no repositório sejam propagadas rapidamente.
</p>

<h2>Explicação sobre o método</h2>

<p>As ativações são feitas usando ssh. O servidor-push faz um ssh para a conta
de espelho do servidor de destino usando uma autenticação de chave pública. A
chave é definida de tal modo que esta ação somente possa ativar uma execução de
espelhamento, nenhum comando mais. O servidor de destino então executa
ftpsync para atualizar o repositório usando rsync como de costume.
<br />
A troca de chaves públicas e o acesso potencial para os servidores rsync
restritos requerem a coordenação entre um(a) operador(a) de espelho e sua
fonte de origem.
</p>

<h2>Configurando um espelho de cliente push</h2>

<p>Para se tornar um cliente push para o repositório FTP, você deverá definir o
espelhamento usando nosso conjunto de script padrão
<a href="ftpmirror#how">ftpsync</a>.
<br />
Uma vez que esteja funcionando, adicione a chave pública sshkey do seu espelho
de origem em seu <code>~&lt;usuário&gt;/.ssh/authorized_keys</code> com uma
restrição <code>command="~/bin/ftpsync</code>. (Você pode ter o ftpsync em um
diretório diferente, faça as adaptações adequadamente).
</p>

<h2>Sites cliente Push-Primary</h2>

<p>Espelhos de cliente Push-Primary, também referidos como espelhos Tier-1, são
os espelhos de cliente push que sincronizam diretamente da rede syncproxy
interna do Debian.
</p>

<p>Se seu site é <strong>muito</strong> bem conectado (com uma largura de banda
muito boa e está conectado com os maiores backbones) e você deseja permitir
que outros sites espelhem a partir do seu site, você pode querer nos contar
para que nós consideremos torná-lo um espelho push. Por favor, contate a equipe
de espelhamento do Debian para detalhes de configuração. Observe,
contudo, que não podemos aceitar todas as solicitações para se tornar um
espelho primário push já que nós já temos um bom número de espelhos Tier-1.
</p>

<h2>Configurando um espelho de servidor push</h2>

<p>Dado o grande número de espelhos e o tamanho do repositório do Debian, não é
factível que todos os espelhos usem os syncproxies internos do Debian como sua
fonte de origem do Debian. É muito mais eficiente se o carregamento for
distribuído entre uma quantidade de espelhos push distribuídos ao redor do
globo.
</p>

<p>Portanto, um número de sites Push-Primary são, por sua vez, servidores push
para seus destinos. Se você quer configurar seu site como um servidor push para
seus sites de destino, veja os
 <a href="push_server">detalhes para configurar um servidor push</a>.
</p>
